<br>Update weight goal<br>
        <button onclick="updateWeight()">Weight Goal</button>                 
        <div id="updateWeight" style="display:none"> 

            <form id ="f1" name="input" action="" method="post">
                <fieldset>
                    <legend>Update Weight Goal:</legend>
                    Goal Type <br>
<br>
                                Goal Name <br>
                                <input id="input2" type="text" name="targetTime" size="30"><br>        
                                <br>
                        Target Weight Loss/Gain <br>
                        <select name="WeightU">
                            <option> - Weight - </option>
                            <option value="0.1">100 g</option>
                            <option value="0.2">200 g</option>
                            <option value="0.3">300 g</option>
                            <option value="0.4">400 g</option>
                            <option value="0.5">500 g</option>
                            <option value="0.6">600 g</option>
                            <option value="0.7">700 g</option>
                            <option value="0.8">800 g</option>
                            <option value="0.9">900 g</option>
                            <option value="1">1.0 kg</option>
                            <option value="1.1">1.1 kg</option>
                            <option value="1.2">1.2 kg</option>
                            <option value="1.3">1.3 kg</option>
                            <option value="1.4">1.4 kg</option>
                            <option value="1.5">1.5 kg</option>
                            <option value="1.6">1.6 kg</option>
                            <option value="1.7">1.7 kg</option>
                            <option value="1.8">1.8 kg</option>
                            <option value="1.9">1.9 kg</option>
                            <option value="2">2.0 kg</option>
                            <option value="2.1">2.1 kg</option>
                            <option value="2.2">2.2 kg</option>
                            <option value="2.3">2.3 kg</option>
                            <option value="2.4">2.4 kg</option>
                            <option value="2.5">2.5 kg</option>
                            <option value="2.6">2.6 kg</option>
                            <option value="2.7">2.7 kg</option>
                            <option value="2.8">2.8 kg</option>
                            <option value="2.9">2.9 kg</option>
                            <option value="3.0">3.0 kg</option>
                            <option value="3.1">3.1 kg</option>
                            <option value="3.2">3.2 kg</option>
                            <option value="3.3">3.3 kg</option>
                            <option value="3.4">3.4 kg</option>
                            <option value="3.5">3.5 kg</option>
                            <option value="3.6">3.6 kg</option>
                            <option value="3.7">3.7 kg</option>
                            <option value="3.8">3.8 kg</option>
                            <option value="3.9">3.9 kg</option>
                            <option value="4">4.0 kg</option>
                            <option value="4.1">4.1 kg</option>
                            <option value="4.2">4.2 kg</option>
                            <option value="4.3">4.3 kg</option>
                            <option value="4.4">4.4 kg</option>
                            <option value="4.5">4.5 kg</option>
                            <option value="4.6">4.6 kg</option>
                            <option value="4.7">4.7 kg</option>
                            <option value="4.8">4.8 kg</option>
                            <option value="4.9">4.9 kg</option>
                            <option value="5">5.0 kg</option>
                        </select>


                        </select>
                        <br>
                        Target Date <br>
                        <select name="DayWU">
                            <option> - Day - </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select>
                        <select name="MonthWU">
                            <option> - Month - </option>
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        <select name="YearWU">
                            <option> - Year - </option>
                            <option value="1993">2015</option>
                            <option value="1992">2016</option>
                            <option value="1991">2017</option>
                            <option value="1990">2018</option>
                            <option value="1989">2019</option>
                            <option value="1988">2020</option>
                            <option value="1987">2021</option>


                        </select>
                        <br>
                        Target achieved<br>
                        <select name="AchievedWU">
                            <option> - achieved - </option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>

                        </select>
                        <br>

                        <br>
                        <input id="input7" name="signUpButton" type="submit" value="Set Goal!">
                        </fieldset> 
                        </form>
                    </div> 

					  <br>Update Exercise<br>
                    <button onclick="updateExercise()">Update Exercise</button>
                    <div id="updateExercise" style="display:none"> 
                        <form id ="f1" name="input" action="" method="post">
                            <fieldset>
                                <legend>Update Exercise: </legend>
                                <br>
                                Exercise Name <br>
                                <input id="input2" type="text" name="targetTime" size="30"><br>        
                                <br>
                                <select name="ExerciseU">
                                    <option> - Exercise - </option>
                                    <option value="walk">Walk</option>
                                    <option value="run">Run</option>
                                    <option value="cycling">Cycling</option>
                                    <option value="rowing">Rowing</option>
                                    <option value="swimming">Swimming</option>
                                </select><br>

                                <select name="DistanceU">
                                    <option> - Distance - </option>
                                    <option value="100">100 m</option>
                                    <option value="200">200 m</option>
                                    <option value="400">300</option>
                                    <option value="400">400</option>
                                    <option value="800">5</option>
                                    <option value="1200">6</option>
                                    <option value="1500">7</option>
                                    <option value="1600">8</option>
                                    <option value="2100">9</option>
                                    <option value="2400">10</option>
                                    <option value="5000">11</option>
                                    <option value="10000">12</option>
                                    <option value="21?097">Half Marathon</option>
                                    <option value="42?195">Marathon</option>
                                </select>
                                <br>
                                Target time<br>
                                <input id="input2" type="time" name="targetTime" size="30"><br>        
                                <br>
                                <br>
                                Completed time<br>
                                <input id="input2" type="time" name="targetTime" size="30"><br>        
                                <br>
                                Completion Date <br>
                                <select name="DayEU">
                                    <option> - Day - </option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                                <select name="MonthEU">
                                    <option> - Month - </option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <select name="YearEU">
                                    <option> - Year - </option>
                                    <option value="1993">2015</option>
                                    <option value="1992">2016</option>
                                    <option value="1991">2017</option>
                                    <option value="1990">2018</option>
                                    <option value="1989">2019</option>
                                    <option value="1988">2020</option>
                                    <option value="1987">2021</option>
                                </select>
                                <br>
                                Time taken: <input id="input2" type="time" name="Time" size="30"><br>
                                <br>Target achieved<br>
                                <select name="AchievedEU">
                                    <option> - achieved - </option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>

                                </select>                                                  

                                <input id="input7" name="signUpButton" type="submit" value="Set Goal!">

                            </fieldset>
                        </form>
                    </div>  