/**
 * This class will be used to create and store a diet which is made up of different
 * food and drink objects. This will have methods etc for total calories of the 
 * meal and things like that, getter and setters. Could split this up and have 
 * a diet that is made up of meal objects and a meal class that is made up of food 
 * and drink object.
 */
package SoftwareEngineering;

import static SoftwareEngineering.Food.rs;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;

/**
 *
 * @author michaelcoulson
 */
public class DietCapture {
    
    private String name;
    private String food;
    private String drink;
    private double totalCalories;
    private ArrayList<Food> fa;
    private ArrayList<Drink> da;
    static ResultSet rs = null;
    
    //Constructor
    public DietCapture(String name, String food, String drink, double calories){
        this.name = name;
        this.food = food;
        this.drink = drink;
        this.totalCalories = calories;
        this.da = new ArrayList<Drink>();
        this.fa = new ArrayList<Food>();
    }
    
    public DietCapture(){
    
    }
    
    public DietCapture(ResultSet rs) throws SQLException {
        this.name = rs.getString("mealname");
        this.food = rs.getString("food_one");
        this.drink = rs.getString("drink_one");
        this.totalCalories = rs.getDouble("calories");
    }
    
    
    public void setF(String f) {
        this.food = f;
    }

    public void setD(String d) {
        this.drink = d;
    }

    public String getF() {
        return this.food;
    }

    public String getD() {
        return this.drink;
    }
    
    /**
     * gets the name
     * @return 
     */
    public String getName() {
        return name;
    }

    /**
     * gets the foods
     * @return 
     */
    public ArrayList<Food> getFa() {
        return fa;
    }

    /**
     * gets the drinks
     * @return 
     */
    public ArrayList<Drink> getDa() {
        return da;
    }

    /**
     * sets the name
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * sets the food array
     * @param fa 
     */
    public void setFa(ArrayList<Food> fa) {
        this.fa = fa;
    }

    /**
     * sets the drink array
     * @param da 
     */
    public void setDa(ArrayList<Drink> da) {
        this.da = da;
    }
    
    /**
     * adds a food
     * @param f 
     */
    public void addFood(Food f){
        this.fa.add(f);
    }
    
    /**
     * Adds a drink
     * @param d 
     */
    public void addDrink(Drink d){
        this.da.add(d);
    }
    
    /**
     * return a specific food
     * @param i
     * @return 
     */
    public Food getFood(int i){
        return this.fa.get(i);
    }
    
    /**
     * return a specific drink
     * @param i
     * @return 
     */
    public Drink getDrink(int i){
        return this.da.get(i);
    }
    
    /**
     * Method that gets the total calories for a meal/diet
     */
    public double getTotalCals(){
        return this.totalCalories;
    }
    
    /**
     * Method used to return all of the meals held for a user 
     * @param search
     * @return
     * @throws ServletException
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
      public static ArrayList returnAll(int search) throws ServletException, SQLException, ClassNotFoundException
    {
        ArrayList dcList = new ArrayList();
            String searchQuery;
        
                searchQuery = "select * from mealcapture where theuseruser_id = '" + search + "';";
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement(searchQuery);
            rs = ps.executeQuery();
            while (rs.next()) 
            {
                DietCapture dc = new DietCapture(rs);
                dcList.add(dc);
            }
        return dcList;
    }
    
}
