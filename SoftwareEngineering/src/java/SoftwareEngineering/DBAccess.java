package SoftwareEngineering;

import java.sql.*;

public class DBAccess 
{
    //  Creates a connection to the database.
    public static Connection getConnection()
            throws ClassNotFoundException, SQLException 
    {
        String jdbcDriver = "org.postgresql.Driver";
        Class.forName(jdbcDriver);

        String jdbcUrl = "jdbc:postgresql://localhost:5432/studentdb";
        String username = "student";
        String password = "";

        return (DriverManager.getConnection(jdbcUrl, username, password));
    }
}
