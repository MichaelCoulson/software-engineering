/**
 * Goal will be the parent class of individual goal and group goal, it makes
 * sense to do it like this then we will minimise code reuse.
 */
package SoftwareEngineering;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;

/**
 *
 * @author michaelcoulson
 */
public class WeightGoal {

    private int goalID;
    private String name;
    private double target;
    private String targetDate;
    private boolean goalAchieved;
    private String completionDate;

    public WeightGoal() {

    }

    public WeightGoal(String name, double target, String targetDate) {
        this.name = name;
        this.target = target;
        this.targetDate = targetDate;
        this.goalAchieved = false;
    }

    public WeightGoal(ResultSet rs) throws SQLException {
        this.goalID = rs.getInt("user_goal_id");
        this.name = rs.getString("goal_name");
        this.target = rs.getDouble("targetweight");
        this.targetDate = rs.getString("targetdate");
        this.goalAchieved = rs.getString("status").equals("true");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTarget() {
        return target;
    }

    public void setTarget(double target) {
        this.target = target;
    }

    public String getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(String targetDate) {
        this.targetDate = targetDate;
    }

     public boolean isGoalAchieved() {
        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("select targetweight, user_id from user_goal where user_goal_id = '" + goalID + "' and status ='false'");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                User weightUser = User.returnUserID(rs.getInt(2));
                if (rs.getDouble(1) >= User.returnUserID(rs.getInt(2)).getWeight()) {
                    ps = con.prepareStatement("update user_goal set completion_date = '"
                            + date + "', status = 'true' WHERE user_goal_id = '" + goalID + "'");
                    ps.executeUpdate();
                    try {
                        ps = con.prepareStatement("select group_id from group_user where user_id = '" + rs.getInt(2) + "'");
                        ResultSet rsUserGroup = ps.executeQuery();

                        while (rsUserGroup.next()) {
                            if (!rs.wasNull()) {
                                int groupID = rs.getInt(1);
                                ps = con.prepareStatement("select user_id from group_user where group_id = '" + groupID + "'");
                                ResultSet rsGroupUsers = ps.executeQuery();
                                while (rsGroupUsers.next()) {
                                    System.out.println("Sending to "+ User.returnUserID(rsGroupUsers.getInt(1)).getEmailAddress());
                                    String recipient = User.returnUserID(rsGroupUsers.getInt(1)).getEmailAddress();
                                    String subject = weightUser.getFirstName() + "Just completed a goal!";
                                    String content = "Just letting you know that " + weightUser.getFirstName() + "Completed there goal!";
                                    EmailUtility.sendEmail("smtp.gmail.com", "587", "ueahealthtracker@gmail.com", "ueapassword", recipient, subject,
                                            content);
                                }
                            }
                        }
                    } catch (MessagingException ex) {
                    }
                    goalAchieved = true;
                    return goalAchieved;
                }
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(ExerciseGoal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ExerciseGoal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(WeightGoal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return goalAchieved;
    }

    public void setGoalAchieved(boolean goalAchieved) {
        this.goalAchieved = goalAchieved;
    }

    public String getCompletionDate() {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("select completion_date from user_goal where user_goal_id = '" + goalID + "' and status = 'true'");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                if (!rs.getString(1).equals("")) {
                    return rs.getString(1);
                }
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(ExerciseGoal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ExerciseGoal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Not Completed";
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public static WeightGoal getWeightGoal(int id) {
        WeightGoal thisGoal = new WeightGoal();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from user_goal where user_goal_id = '" + id + "'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                thisGoal = new WeightGoal(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ExerciseGoal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ExerciseGoal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return thisGoal;
    }

}
