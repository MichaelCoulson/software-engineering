package SoftwareEngineering;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Will
 */
public class foodList extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int query = 1;
            ArrayList Foods = Food.returnAll(query);
            request.setAttribute("foodList", Foods);
            request.getRequestDispatcher("newjsp.jsp").forward(request, response);
            
        } catch (SQLException ex) {
            Logger.getLogger(foodList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(foodList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
