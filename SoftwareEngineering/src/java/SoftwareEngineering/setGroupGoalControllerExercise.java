package SoftwareEngineering;


import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ptj12sdu
 */
public class setGroupGoalControllerExercise extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        try {
            HttpSession session = request.getSession(true);
            Group goalGroup = (Group) session.getAttribute("groupSession");
            String day, month, year, targetDate;

            String nameE = request.getParameter("exerciseName");
            String groupName = goalGroup.getGroupName();
            String type = request.getParameter("Exercise");
            double dist = Double.parseDouble(request.getParameter("Distance"));
            double dur = Double.parseDouble(request.getParameter("Duration"));
            double time = Double.parseDouble(request.getParameter("targetTime"));
            day = request.getParameter("DayE");
            month = request.getParameter("MonthE");
            year = request.getParameter("YearE");
            targetDate = day + "/" + month + "/" + year;
            
            GroupExerciseGoal eg = new GroupExerciseGoal(groupName, nameE, targetDate, type, dist,dur,time);
            goalGroup.exerciseGoalUpdate(eg);
            
            session.setAttribute("groupSession", goalGroup);
            response.sendRedirect("ProfilePage.jsp");
        }
        catch(IOException e){
            System.out.println("Error IO");
        } catch (NumberFormatException e) {
            System.out.println("Error ");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(setGoalControllerExercise.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(setGoalControllerExercise.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
