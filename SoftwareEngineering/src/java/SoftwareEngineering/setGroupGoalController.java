package SoftwareEngineering;


import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class setGroupGoalController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        try {
            HttpSession session = request.getSession(true);
            Group goalGroup = (Group) session.getAttribute("groupSession");
            String day, month, year, targetDate;
            
            String goalName = request.getParameter("weightName");
            
            Double weightGoal = Double.parseDouble(request.getParameter("weightGoal"));
            String groupName = goalGroup.getGroupName();
            day = request.getParameter("DayW");
            month = request.getParameter("MonthW");
            year = request.getParameter("YearW");
            targetDate = "" + day + "/" + month + "/" + year + "";
            
            GroupGoal gwg = new GroupGoal(groupName, goalName,weightGoal,targetDate);
            goalGroup.weightGoalUpdate(gwg);
            
            session.setAttribute("groupSession", goalGroup);
            response.sendRedirect("ProfilePage.jsp");
        }
        catch(IOException e){
            System.out.println("Error IO");
        } catch (NumberFormatException e) {
            System.out.println("Error number");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(setGoalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(setGoalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(setGroupGoalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
