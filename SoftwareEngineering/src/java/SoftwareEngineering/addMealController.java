/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SoftwareEngineering;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rah12nyu
 */
public class addMealController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      //  processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        try {
            //  used to edit the user profile.
            HttpSession session = request.getSession(true);
            User user = (User) (session.getAttribute("userSession"));

            //Is it breakfast, lunch, dinner or a snack.
            String mealType = request.getParameter("mealName");   
            String drink = request.getParameter("dName");
            String food = request.getParameter("fName");
            
            String delims = "[,]";
            String[] drinkTokens = drink.split(delims);
            String[] foodTokens = food.split(delims);
            
            drink = drinkTokens[0];
            food = foodTokens[0];
 
            double calories = Double.parseDouble(drinkTokens[1]) + 
                    Double.parseDouble(foodTokens[1]);

            
            
            DietCapture dc = new DietCapture(mealType, food, drink, calories);
            user.dietCaptureUpdate(dc);
            
            session.setAttribute("userSession", user);
            response.sendRedirect("ProfilePage.jsp");
        }
        catch(IOException e){
            System.out.println("Error");
        } catch (NumberFormatException e) {
            System.out.println("Error");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(setGoalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(setGoalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
