package SoftwareEngineering;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class editController extends HttpServlet
{

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        try {
            //  used to edit the user profile.
            HttpSession session = request.getSession(true);
            User user = (User) (session.getAttribute("userSession"));
            double weight = Double.parseDouble(request.getParameter("weight"));
            double height = Double.parseDouble(request.getParameter("height"));
            user.setWeight(weight);
            user.setHeight(height);
            
            user.update();
            Date d = new Date();
            SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
            System.out.println(dt1.format(d));
            user.updatehistory(dt1.format(d));
            user.setBmi(user.calculateBMI(weight, height));
            
            //Used to add to the history when the weight and height have been
            //updated
            user.addHeight(height);
            user.addWeight(weight);
            user.addBMI(user.calculateBMI(weight, height));
            
            session.setAttribute("userSession", user);
            response.sendRedirect("ProfilePage.jsp");
        } catch (SQLException ex) {
            System.out.println("hello");
            Logger.getLogger(editController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            System.out.println("goodvye");
            Logger.getLogger(editController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
