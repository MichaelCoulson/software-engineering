/**
 * This class is used to create a group which is made up of individual users,
 * They will be able to set goal and send messages to each other.
 */
package SoftwareEngineering;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;

/**
 *
 * @author Bradley
 */
public class Group {

    private String groupName;
    private ArrayList<User> users;
    private ArrayList<WeightGoal> groupGoals;
    private ArrayList<GroupGoal> wGoals;
    private String catagory;
    private int rating;
    static ResultSet rs = null;
    private ArrayList<newMessage> groupMessages;
    private ArrayList<GroupExerciseGoal> Egoal;

    /**
     * A Constructor for a Group
     *
     * @param name the name of the group
     * @param cat
     */
    public Group(String name, String cat, int rat) {
        this.groupName = name;
        this.catagory = cat;
        this.rating = rat;
        this.users = new ArrayList<User>();
        this.groupMessages = new ArrayList<newMessage>();
        this.groupGoals = new ArrayList<WeightGoal>();
        this.wGoals = new ArrayList<GroupGoal>();
    }

    /**
     * A Default constructor for the group objects
     *
     * @author John.
     */
    public Group() {
        this.groupName = null;
        this.catagory = null;
        this.rating = 0;
        this.users = new ArrayList<User>();
        this.groupMessages = new ArrayList<newMessage>();
        this.groupGoals = new ArrayList<WeightGoal>();
        this.wGoals = new ArrayList<GroupGoal>();
    }

    /**
     * A Method which creates a user object from a result set. Used when adding
     * object to sessions so site personalisation can take place.
     *
     * @param rs the result set to create a group from.
     * @author John
     * @throws java.sql.SQLException
     */
    public Group(ResultSet rs) throws SQLException {
        this.groupName = rs.getString("group_name");
        this.catagory = rs.getString("catagory");
        this.rating = rs.getInt("rating");
        this.users = new ArrayList<User>();
        this.groupMessages = new ArrayList<newMessage>();
        this.groupGoals = new ArrayList<WeightGoal>();
        this.wGoals = new ArrayList<GroupGoal>();
        this.Egoal = new ArrayList<GroupExerciseGoal>();
    }

    /**
     * gets group name
     *
     * @return
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * sets group name
     *
     * @param groupName
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * set user
     *
     * @param users
     */
    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    /**
     * set group goals
     *
     * @param groupGroals
     */
    public void setGroupGoals(ArrayList<WeightGoal> groupGroals) {
        this.groupGoals = groupGroals;
    }

    public String getCatagory() {
        return catagory;
    }

    public void setCatagory(String catagory) {
        this.catagory = catagory;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    /**
     *
     * @return @author John
     */
    public ArrayList<User> getUsers() {
        //quickfix to stop duplicates
        users = new ArrayList<User>();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT user_id FROM group_user WHERE group_id = '" + getGroupID(groupName) + "' AND status = 'y'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                this.users.add(User.returnUserID(rs.getInt(1)));
            }
        } catch (ServletException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        }
        return users;
    }

    /**
     * adds a user to the user arraylist.
     *
     * @param u
     */
    public void AddUser(User u) {
        this.users.add(u);
    }
    
    public void updateRating(){
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("update grouptbl set rating = (rating+1) where group_id = '" + getGroupID(groupName) + "'" );
            ps.executeUpdate();
            con.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateExerciseGoal(int exerciseID,  String distance) throws ServletException {
        double distanceInt = Double.parseDouble(distance);
        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("update groupexercise set distance=(distance-" + distanceInt + ") where group_id = '"
                    + getGroupID(groupName) + "' and exercise_id = '" + exerciseID + "' and status = 'false'");
            ps.executeUpdate();
            if (goalAchieved(exerciseID) == true) {
                PreparedStatement psUpdate = con.prepareStatement("update groupexercise set status='true', completiondate='"+date+"' where group_id = '"
                        + getGroupID(groupName) + "' and exercise_id = '" + exerciseID + "' and status = 'false'");
                psUpdate.executeUpdate();
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean goalAchieved(int exerciseID) throws ServletException {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT distance from groupexercise where group_id ='"
                    + getGroupID(groupName) + "' and exercise_id = '" + exerciseID + "'");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                if (rs.getInt(1) <= 0) {
                    updateRating();
                    return true;
                }
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    public ArrayList<ExerciseGoal> updateExerciseGoals(int userID) throws SQLException {
        ArrayList<ExerciseGoal> exerciseGoal = new ArrayList();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("select exercise_id from exercise where user_id ='" + userID + "' and status = 'true'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                exerciseGoal.add(ExerciseGoal.getExerciseGoal(rs.getInt(1)));
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return exerciseGoal;
    }

    public ArrayList<WeightGoal> updateWeightGoals(int userID) throws SQLException {
        ArrayList<WeightGoal> weightGoal = new ArrayList();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("select user_goal_id from user_goal where user_id ='" + userID + "' and status = 'true'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                weightGoal.add(WeightGoal.getWeightGoal(rs.getInt(1)));
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return weightGoal;
    }

    /**
     * A Method which gets the groupID of a group
     *
     * @param name the name of which we should get a group id
     * @return the id of the group
     * @author John
     */
    public int getGroupID(String name) throws ServletException {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT group_id FROM grouptbl WHERE group_name ='" + groupName + "'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            throw new ServletException("Persist Problem", e);
        }
        return 0;
    }

    /**
     * A Method which adds a new group to the database. Checks to ensure that
     * the group name is unique before passing through.
     *
     * @throws ServletException
     * @author John
     */
    public void persist() throws ServletException {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("INSERT INTO grouptbl VALUES (?, ?,?,?)");
            PreparedStatement psTwo = con.prepareStatement("SELECT MAX(group_id)+1 FROM grouptbl");
            ResultSet rs = psTwo.executeQuery();

            while (rs.next()) {
                int maxValue = rs.getInt(1);
                ps.setInt(1, maxValue);
                ps.setString(2, groupName);
                ps.setString(3, catagory);
                ps.setInt(4, rating);
                ps.executeUpdate();
            }
            con.close();
        } catch (ClassNotFoundException e) {
            throw new ServletException("Persist Problem", e);
        } catch (SQLException e) {
            throw new ServletException("Persist Problem", e);
        }
    }

    /**
     *
     * @return
     */
    public ArrayList<newMessage> getMessages() {
        groupMessages.clear();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT group_useruser_id,"
                    + " comment FROM group_user_messages"
                    + " WHERE group_usergroup_id = '" + getGroupID(groupName) + "'");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int poster = rs.getInt(1);
                String message = rs.getString(2);
                newMessage thisMessage = new newMessage(poster, message);
                groupMessages.add(thisMessage);
            }
            con.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        }
        Collections.reverse(groupMessages);
        return groupMessages;
    }

    /**
     * A Method which adds both the group and the user to the 'group user' table
     * Stores which users are members of which groups.
     *
     * @author John
     * @param group the group to store
     * @param user the user to store.
     * @param status the status of the user in respects to the group.
     *
     */
    public static void updateUserAndGroup(Group group, User user, String status) {
        try {
            Connection con = DBAccess.getConnection();
            int groupID = group.getGroupID(group.getGroupName());
            int userID = user.getUserID();
            int count = 0;
            PreparedStatement psCount = con.prepareStatement("SELECT COUNT(*) "
                    + "FROM group_user WHERE group_id ='" + groupID + "' AND "
                    + "user_id ='" + userID + "'");
            ResultSet countResult = psCount.executeQuery();
            if (countResult.next()) {
                count = countResult.getInt(1);
            }

            if (count == 0) {
                PreparedStatement ps = con.prepareStatement("INSERT INTO group_user VALUES (?, ?, ?)");
                ps.setInt(1, groupID);
                ps.setInt(2, userID);
                ps.setString(3, status);
                ps.executeUpdate();
                con.close();
            } else {
                PreparedStatement ps = con.prepareStatement("UPDATE group_user"
                        + " SET status='" + status + "' WHERE user_id = '" + userID + "'"
                        + " AND group_id = '" + groupID + "';");
                ps.executeUpdate();
                con.close();
            }
        } catch (ServletException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * A Method which checks to see if a group with the same name exists This
     * should stop any errors from happening when SQL tries adding duplicates to
     * the server.
     *
     * @return false if there is a group with the same name true if there is no
     * duplication.
     * @throws ServletException
     * @author John
     */
    public boolean check() throws ServletException {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement nameCheck = con.prepareCall("SELECT * FROM grouptbl WHERE group_name ='" + groupName + "'");
            ResultSet rs = nameCheck.executeQuery();
            con.close();
            if (rs.next()) {
                if (rs.getString("group_name").equals(groupName)) {
                    return false;
                }
            }
        } catch (Exception e) {
            throw new ServletException("Persist Problem", e);
        }
        return true;
    }

    /**
     * A method which gets a group using the name of the group.
     *
     * @param name the name of the group to find
     * @author John
     * @return the group that was found.
     */
    public static Group returnGroup(String name) throws ServletException {
        Group thisGroup = new Group();

        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM grouptbl WHERE group_name ='" + name + "'");

            ResultSet rs = ps.executeQuery();
            con.close();
            if (rs.next()) {
                if (rs.getString("group_name").equals(name)) {
                    thisGroup = new Group(rs);
                    return thisGroup;
                }
            }
        } catch (Exception e) {
            throw new ServletException("returning Group problem", e);
        }
        return thisGroup;
    }

    /**
     * A method which gets a group using the id of the group.
     *
     * @param groupID the id of the group to find
     * @author John
     * @return the group that was found.
     */
    public static Group returnGroupID(int groupID) throws ServletException {
        Group thisGroup = new Group();

        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM grouptbl WHERE group_id ='" + groupID + "'");

            ResultSet rs = ps.executeQuery();
            con.close();
            if (rs.next()) {
                if (rs.getInt("group_id") == groupID) {
                    thisGroup = new Group(rs);
                    return thisGroup;
                }
            }
        } catch (Exception e) {
            throw new ServletException("returning Group problem", e);
        }
        return thisGroup;
    }

    /**
     * A Method which adds a comment to the page.
     *
     * @author John
     * @param group the group in which the message is added
     * @param user the user who posted the message
     * @param message the message the user wishes to post
     * @throws javax.servlet.ServletException
     */
    public static void addUserMessage(Group group, User user, String message) throws ServletException {
        try {
            int maxValue = 0;
            Connection con = DBAccess.getConnection();
            PreparedStatement psMax = con.prepareStatement("SELECT MAX(commentid)+1 "
                    + "FROM group_user_messages");
            ResultSet results = psMax.executeQuery();
            if (results.next()) {
                maxValue = results.getInt(1);
            }

            PreparedStatement ps = con.prepareStatement("INSERT INTO group_user_"
                    + "messages (group_usergroup_id, group_useruser_id, commentid ,"
                    + "comment)"
                    + "VALUES ('" + group.getGroupID(group.getGroupName()) + "',"
                    + "'" + user.getUserID() + "'," + maxValue + ",'" + message + "')");
            ps.execute();
            con.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public class newMessage {

        private final int messagePoster;
        private final String message;

        public newMessage(int messagePoster, String message) {
            this.messagePoster = messagePoster;
            this.message = message;
        }

        public int getMessagePoster() {
            return this.messagePoster;
        }

        public String getMessage() {
            return this.message;
        }
    }

    public static ArrayList returnAll(String search, String type) throws ServletException, SQLException, ClassNotFoundException {
        ArrayList groupList = new ArrayList();
        String searchQuery;
        if (search.isEmpty()) {
            if (type.compareTo("All") == 0) {
                searchQuery = "select * from grouptbl;";
            } else {
                searchQuery = "select * from grouptbl where catagory = '" + type + "';";
            }
        } else {
            if (type.compareTo("All") == 0) {
                searchQuery = "select * from grouptbl where group_name = '" + search + "';";

            } else {
                searchQuery = "select * from grouptbl where group_name = '" + search + "' and catagory = '" + type + "';";
            }
        }
        Connection con = DBAccess.getConnection();
        PreparedStatement ps = con.prepareStatement(searchQuery);
        rs = ps.executeQuery();
        while (rs.next()) {
            Group g = new Group(rs);
            groupList.add(g);
        }
        return groupList;
    }

    public ArrayList<GroupGoal> getGroupWeightGoals() {
        return this.wGoals;
    }

    public ArrayList<GroupExerciseGoal> getEgoal() {
        return this.Egoal;
    }

    public void updateWeightGoals() throws SQLException {
        wGoals.clear();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("select group_goal_id from group_goal where group_id ='" + getGroupID(groupName) + "'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                if (!rs.wasNull()) {
                    this.wGoals.add(GroupGoal.getWeightGoal(rs.getInt(1)));
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void weightGoalUpdate(GroupGoal g) throws ClassNotFoundException, SQLException, ServletException, MessagingException {
        Connection con = DBAccess.getConnection();
        PreparedStatement ps = con.prepareStatement("INSERT INTO group_goal "
                + "(group_goal_id,group_id,goalname,target_value,status,target_date,completion_date)"
                + "VALUES(?,?,?,?,?,?,?);");
        PreparedStatement psTwo = con.prepareStatement("SELECT MAX(group_goal_id)+1 FROM group_goal");
        ResultSet rs = psTwo.executeQuery();
        int maxValue = 0;
        if (rs.next()) {
            maxValue = rs.getInt(1);
        }
        ps.setInt(1, maxValue);
        ps.setInt(2, getGroupID(groupName));
        ps.setString(3, g.getGoalName());
        ps.setDouble(4, g.getWeightGoal());
        ps.setString(5, Boolean.toString(g.isGoalAchieved()));
        ps.setString(6, g.getTargetDate());
        ps.setString(7, "");
        ps.executeUpdate();
        con.close();
    }

    public void exerciseGoalUpdate(GroupExerciseGoal e) throws ClassNotFoundException, SQLException, ServletException {
        Connection con = DBAccess.getConnection();
        PreparedStatement ps = con.prepareStatement("INSERT INTO groupexercise "
                + "(group_id, exercise_id,exercisename,typeofexercise,distance,duration,targettime,completiontime,targetdate,completiondate,status)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?);");
        PreparedStatement psTwo = con.prepareStatement("SELECT MAX(exercise_id)+1 FROM groupexercise");
        ResultSet rs = psTwo.executeQuery();
        int maxValue = 0;
        if (rs.next()) {
            maxValue = rs.getInt(1);
        }
        ps.setInt(1, getGroupID(groupName));
        ps.setInt(2, maxValue);
        ps.setString(3, e.getGoalName());
        ps.setString(4, e.getGoalType());
        ps.setDouble(5, e.getGoalDistace());
        ps.setDouble(6, e.getGoalDuration());
        ps.setDouble(7, e.getGoalTime());
        ps.setDouble(8, e.getGoalCompletionTime());
        ps.setString(9, e.getTargetDate());
        ps.setString(10, "");
        ps.setString(11, Boolean.toString(e.isGoalAchieved()));
        ps.executeUpdate();
        con.close();
    }

    /**
     * A method which updates the arraylist with all of the current goals that
     * the person is doing.
     *
     * @throws SQLException
     */
    public void updateExerciseGoals() throws SQLException, ServletException {
        this.Egoal.clear();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT exercise_id from groupexercise where group_id ='" + getGroupID(groupName) + "'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                this.Egoal.add(GroupExerciseGoal.getExerciseGoal(rs.getInt(1)));
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
