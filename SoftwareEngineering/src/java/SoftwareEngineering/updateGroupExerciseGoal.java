package SoftwareEngineering;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * SignUpServlet.java is used to sign up a new user.
 *
 * @author michaelcoulson
 */
public class updateGroupExerciseGoal extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(true);
        Group group = (Group) (session.getAttribute("groupSession"));
        int goalID = Integer.parseInt(request.getParameter("groupID"));
        String distance = request.getParameter("distanceMoved");

        group.updateExerciseGoal(goalID, distance);
        session.setAttribute("groupSession", group);
        response.sendRedirect("ProfilePage.jsp");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
