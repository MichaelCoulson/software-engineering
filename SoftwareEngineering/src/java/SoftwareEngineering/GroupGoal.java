package SoftwareEngineering;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;

/**
 *
 * @author michaelcoulson
 */
public class GroupGoal {

    private int goalID;
    private int groupID;
    private boolean status;
    private String groupName;
    private String goalName;
    private double weightGoal;
    private String targetDate;
    private ArrayList<User> user;
    Group g;

    public GroupGoal() {

    }

    //Constructor
    public GroupGoal(String groupName, String goalName, double weightGoal, String targetDate) {
        this.goalName = goalName;
        this.groupName = groupName;
        this.weightGoal = weightGoal;
        this.targetDate = targetDate;
    }

    public GroupGoal(ResultSet rs) throws SQLException {
        try {
            this.goalID = rs.getInt("group_goal_id");
            this.groupID = rs.getInt("group_id");
            this.groupName = Group.returnGroupID(rs.getInt("group_id")).getGroupName();
            this.goalName = rs.getString("goalname");
            this.targetDate = rs.getString("target_date");
            this.weightGoal = rs.getInt("target_value");
            this.status = rs.getString("status").equals("true");
        } catch (ServletException ex) {
            Logger.getLogger(GroupGoal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getGoalName() {
        return this.goalName;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public double getWeightGoal() {
        return this.weightGoal;
    }

    public String getTargetDate() {
        return this.targetDate;
    }

    public int getGroupWeight() {

        return 0;
    }

    public boolean isGoalAchieved() throws MessagingException, ServletException {
        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT COALESCE (sum(weight),0) FROM theuser INNER JOIN group_user "
                    + "ON theuser.user_id = group_user.user_id "
                    + "WHERE group_user.group_id = '" + groupID
                    + "' AND group_user.status = 'y'");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                PreparedStatement ps2 = con.prepareStatement("select target_value from group_goal where group_id = '"
                        + groupID + "' and status = 'false'");
                ResultSet rsTotal = ps2.executeQuery();
                if (!rs.wasNull()) {
                    System.out.println(rs.getInt(1));

                    while (rsTotal.next()) {
                        System.out.println(rsTotal.getInt(1));
                        if (rs.getInt(1) <= rsTotal.getInt(1)) {
                            Group.returnGroupID(groupID).updateRating();
                            System.out.println(rs.getInt(1) + " " + rsTotal.getInt(1));
                            ps = con.prepareStatement("update group_goal set completion_date = '"
                                    + date + "', status = 'true' WHERE group_goal_id = '" + goalID + "'");
                            ps.executeUpdate();
                            try {
                                //check statements
                                ps = con.prepareStatement("select group_id from group_user where group_id = '" + groupID + "'");
                                ResultSet rsUserGroup = ps.executeQuery();

                                while (rsUserGroup.next()) {
                                    if (!rsUserGroup.wasNull()) {
                                        if (rsUserGroup.next()) {
                                            ps = con.prepareStatement("select user_id from group_user where group_id = '" + groupID + "' and status = 'y'");
                                            ResultSet rsGroupUsers = ps.executeQuery();
                                            if (rsGroupUsers.next()) {
                                                if (!rsGroupUsers.wasNull()) {
                                                    while (rsGroupUsers.next()) {
                                                        System.out.println("Sending to " + User.returnUserID(rsGroupUsers.getInt(1)).getEmailAddress());
                                                        String recipient = User.returnUserID(rsGroupUsers.getInt(1)).getEmailAddress();
                                                        String subject = Group.returnGroupID(groupID).getGroupName() + "Just completed a goal!";
                                                        String content = "Just letting you know that " + Group.returnGroupID(groupID).getGroupName() + "Completed there goal!";
                                                        EmailUtility.sendEmail("smtp.gmail.com", "587", "ueahealthtracker@gmail.com", "ueapassword", recipient, subject,
                                                                content);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (ServletException ex) {
                                Logger.getLogger(GroupGoal.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            boolean goalAchieved = true;
                            return goalAchieved;
                        }
                        status = true;
                        return true;
                    }
                }
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(GroupGoal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GroupGoal.class.getName()).log(Level.SEVERE, null, ex);
        }

        return status;
    }

    public String getCompletionDate() {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("select completion_date from group_goal where group_goal_id = '" + goalID + "' and status = 'true'");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                if (!rs.getString(1).equals("")) {
                    return rs.getString(1);
                }
            }
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(ExerciseGoal.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ExerciseGoal.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return "Not Completed";
    }

    public static GroupGoal getWeightGoal(int id) {
        GroupGoal thisGoal = new GroupGoal();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from group_goal where group_goal_id = '" + id + "'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                thisGoal = new GroupGoal(rs);

            }
        } catch (SQLException ex) {
            Logger.getLogger(ExerciseGoal.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ExerciseGoal.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return thisGoal;
    }

}
