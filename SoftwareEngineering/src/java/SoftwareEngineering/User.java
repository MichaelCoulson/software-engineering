/**
 * Probably the most important class this is will be used to store all the
 * important information about the user including log in details and there
 * weight and height which will be accessed using a healthOverview object.
 */
package SoftwareEngineering;

import static SoftwareEngineering.Food.rs;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;

public class User {

    private int userID;

    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private double height;
    private double weight;
    private double bmi;
    private String dob;
    private String gender;
    private String emailAddress;
    private boolean valid;
    private int rating;
    private double targetWeight;
    private String lastLogin;
    private ArrayList<Group> userGroups;
    private ArrayList<WeightGoal> Wgoal;
    private ArrayList<ExerciseGoal> Egoal;
    private ArrayList weightHistory;
    private ArrayList heightHistory;
    private ArrayList bmiHistory;
    private ArrayList<DietCapture> mealHistory;
    private DietCapture dc;
    private ArrayList<Group> pendingGroups;
    private ArrayList<Food> foodList;
    private ArrayList<Drink> drinkList;

    public User() {
    }

    public User(String username, String firstName, String lastName, String password, double height, double weight,
            String dob, String gender, String emailAddress, double bmi, int rating, double targetWeight, String lastLogin) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.height = height;
        this.weight = weight;
        this.dob = dob;
        this.rating = rating;
        this.bmi = bmi;
        this.gender = gender;
        this.emailAddress = emailAddress;
        this.targetWeight = targetWeight;
        this.lastLogin = lastLogin;
        this.dc = new DietCapture();
        this.Wgoal = new ArrayList<WeightGoal>();
        this.Egoal = new ArrayList<ExerciseGoal>();
        this.userGroups = new ArrayList<Group>();
        this.weightHistory = new ArrayList();
        this.heightHistory = new ArrayList();
        this.bmiHistory = new ArrayList();
        this.pendingGroups = new ArrayList();
        this.mealHistory = new ArrayList<DietCapture>();
        this.foodList = new ArrayList<Food>();
        this.drinkList = new ArrayList<Drink>();
    }

    /**
     * A Method which creates a user object from a result set. Used when adding
     * object to sessions so site personalisiation can take place.
     *
     * @param rs the results set.
     * @throws SQLException
     */
    public User(ResultSet rs) throws SQLException {
        this.username = rs.getString("username");
        this.password = rs.getString("password");
        this.firstName = rs.getString("first_name");
        this.lastName = rs.getString("last_name");
        this.rating = rs.getInt("rating");
        this.height = rs.getDouble("height");
        this.weight = rs.getDouble("weight");
        this.dob = rs.getString("dob");
        this.gender = rs.getString("gender");
        this.emailAddress = rs.getString("email");
        this.bmi = rs.getDouble("bmi");
        this.targetWeight = rs.getDouble("targetweight");
        this.rating = rs.getInt("rating");
        this.lastLogin = rs.getString("lastLogin");
        this.Wgoal = new ArrayList<WeightGoal>();
        this.Egoal = new ArrayList<ExerciseGoal>();
        this.userGroups = new ArrayList<Group>();
        this.weightHistory = new ArrayList();
        this.heightHistory = new ArrayList();
        this.bmiHistory = new ArrayList();
        this.pendingGroups = new ArrayList();
        this.mealHistory = new ArrayList<DietCapture>();
        this.dc = new DietCapture();
        this.foodList = new ArrayList<Food>();
        this.drinkList = new ArrayList<Drink>();
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    /**
     * Method used to add a weight to the users history
     *
     * @param weight
     */
    public void addWeight(double weight) {
        weightHistory.add(weight);
    }

    /**
     * Method used to add a height to the users history
     *
     * @param height
     */
    public void addHeight(double height) {
        heightHistory.add(height);
    }

    /**
     * Method used to add a bmi to the users history
     *
     * @param bmi
     */
    public void addBMI(double bmi) {
        bmiHistory.add(bmi);
    }

    /**
     * The BMI is calculated as per the nhs guidelines for calculating adult
     * BMI. Divide your weight in kilograms by your height in metres then divide
     * your answer by your height.
     *
     * @param weight
     * @param height
     * @return
     */
    public double calculateBMI(double weight, double height) {
        double answer = weight / height;
        this.bmi = answer / height;
        DecimalFormat df = new DecimalFormat("#0.##");
        this.bmi = Double.parseDouble(df.format(bmi));
        return bmi;
    }

    /**
     * method used to return whether a user is healthy, overweight or
     * underweight
     *
     * @return
     */
    public String returnHealthStatus() {
        String healthiness;
        if (this.bmi > 25) {
            healthiness = "Over Weight!";
        } //Checks if the user is a healthy weight and sets recommended weight to 0.
        else if (this.bmi >= 18.5 && this.bmi <= 25) {
            healthiness = "Healthy!";
        } //checks if the user is underweight and calculates how much weight they
        //need to gain to get a healthy bmi.
        else {
            healthiness = "Under Weight!";
        }
        return healthiness;
    }

    /**
     * used to get the target weight of a user.
     *
     * @return
     */
    public double getTargetWeight() {
        return targetWeight;
    }

    /**
     * This method is used to calculate how much weight a user needs to lose or
     * gain to reach a healthy BMI.
     *
     * @return
     */
    public double generateHealth() {
        double recommendedWeight = 0;
        //Checks if the person is overweight and calculates how much weight they
        //need to lose.
        if (this.bmi > 25) {
            double loss = this.bmi - 25;
            recommendedWeight = (loss * this.height) * this.height;
        } //Checks if the user is a healthy weight and sets recommended weight to 0.
        else if (this.bmi >= 18.5 && this.bmi <= 25) {
            recommendedWeight = 0;
        } //checks if the user is underweight and calculates how much weight they
        //need to gain to get a healthy bmi.
        else {
            double gain = (18.5 * this.height) * this.height;
            recommendedWeight = gain - this.weight;
        }
        //The weight needed to lose of gain.
        DecimalFormat df = new DecimalFormat("#0.##");
        recommendedWeight = Double.parseDouble(df.format(recommendedWeight));
        return recommendedWeight;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public double getBmi() {
        return this.bmi;
    }

    public void setBmi(double bmi) {
        this.bmi = bmi;
    }

    public double getHeight() {
        DecimalFormat df = new DecimalFormat("##.##");
        return Double.parseDouble(df.format(height));
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        DecimalFormat df = new DecimalFormat("##.##");
        return Double.parseDouble(df.format(weight));
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public boolean isValid() {
        return valid;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public ArrayList<Group> getGroups() {
        return this.userGroups;
    }

    public ArrayList<WeightGoal> getWgoal() {
        return Wgoal;
    }

    public ArrayList<ExerciseGoal> getEgoal() {
        return this.Egoal;
    }

    public ArrayList<Group> getPendingGroups() {
        return this.pendingGroups;
    }

    public void addFood(Food f) {
        foodList.add(f);
    }

    public void AddDrink(Drink d) {
        drinkList.add(d);
    }

    public ArrayList<Group> getUserGroups() {
        return userGroups;
    }

    public ArrayList getWeightHistory() {
        return weightHistory;
    }

    public ArrayList getHeightHistory() {
        return heightHistory;
    }

    public ArrayList getBmiHistory() {
        return bmiHistory;
    }

    public ArrayList<DietCapture> getMealHistory() {
        return mealHistory;
    }

    public DietCapture getDc() {
        return dc;
    }

    public ArrayList<Food> getFoodList() {
        return foodList;
    }

    public ArrayList<Drink> getDrinkList() {
        return drinkList;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void updateRating() {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("update theuser set rating = (rating+1) where user_id = '" + getUserID() + "'");
            ps.executeUpdate();
            con.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateGroupExercises(int exerciseID) throws ServletException {
        ExerciseGoal thisGoal = ExerciseGoal.getExerciseGoal(exerciseID);
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("select group_id from group_user where user_id = '" + getUserID() + "'");
            ResultSet rsGetID = ps.executeQuery();
            while (rsGetID.next()) {
                Group thisGroup = Group.returnGroupID(rsGetID.getInt(1));
                PreparedStatement psExercise = con.prepareStatement("select exercise_id from groupexercise where group_id ='" + thisGroup.getGroupID(thisGroup.getGroupName()) + "'"
                        + " and typeofexercise = '" + thisGoal.getType() + "' and status = 'false'");
                ResultSet rsGroup = psExercise.executeQuery();
                while (rsGroup.next()) {
                    thisGroup.updateExerciseGoal(rsGroup.getInt(1), Double.toString(thisGoal.getDistance()));
                }
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateExerciseGoal(int exerciseID, String date, String time) throws ServletException {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("update exercise set completiontime = '" + time
                    + "', status ='true', completiondate = '" + date + "' where exercise_id = '" + exerciseID + "'");
            ps.executeUpdate();
            updateGroupExercises(exerciseID);
            updateRating();
            try {
                ps = con.prepareStatement("select group_id from group_user where user_id = '" + getUserID() + "'");
                ResultSet rsUserGroup = ps.executeQuery();
                while (rsUserGroup.next()) {
                    if (!rsUserGroup.wasNull()) {
                        int groupID = rsUserGroup.getInt(1);
                        ps = con.prepareStatement("select user_id from group_user where group_id = '" + groupID + "'");
                        ResultSet rsGroupUsers = ps.executeQuery();
                        while (rsGroupUsers.next()) {
                            System.out.println("Sending to " + User.returnUserID(rsGroupUsers.getInt(1)).getEmailAddress());
                            String recipient = User.returnUserID(rsGroupUsers.getInt(1)).getEmailAddress();
                            String subject = firstName + " Just completed a goal!";
                            String content = "Just letting you know that " + firstName + " Completed there goal!";
                            EmailUtility.sendEmail("smtp.gmail.com", "587", "ueahealthtracker@gmail.com", "ueapassword", recipient, subject,
                                    content);
                        }
                    }
                }
            } catch (MessagingException ex) {
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * A Method which finds the userID of the person.
     *
     * @return the user id of the person
     * @author John
     * @throws javax.servlet.ServletException
     */
    public int getUserID() throws ServletException {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT user_id FROM theuser WHERE email ='" + emailAddress + "'");

            ResultSet rs = ps.executeQuery();
            con.close();
            if (rs.next()) {
                con.close();
                return rs.getInt(1);
            }

        } catch (Exception e) {
            throw new ServletException("returning user problem", e);

        }
        return 0; //change this to allow remove of this return statement.
    }

    public void updateWeightGoals() throws SQLException {
        this.Wgoal.clear();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("select user_goal_id from user_goal where user_id ='" + getUserID() + "'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                this.Wgoal.add(WeightGoal.getWeightGoal(rs.getInt(1)));
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * A method which updates the arraylist with all of the current goals that
     * the person is doing.
     *
     * @throws SQLException
     */
    public void updateExerciseGoals() throws SQLException {
        this.Egoal.clear();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT exercise_id from exercise where user_id ='" + getUserID() + "'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                this.Egoal.add(ExerciseGoal.getExerciseGoal(rs.getInt(1)));
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * A Method which gets all of the users pending groups and adds them to the
     * arraylist.
     *
     * @throws SQLException
     */
    public void updatePendingGroups() throws SQLException {
        this.pendingGroups.clear();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT group_id FROM group_user WHERE user_id = '" + getUserID() + "' AND status = 'p'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                this.pendingGroups.add(Group.returnGroupID(rs.getInt(1)));
            }
        } catch (ServletException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * A method which gets all of the groups that the user is currently in. Once
     * the method has retrieved all of the groups, adds them to the arraylist.
     *
     * @throws SQLException
     * @author John
     */
    public void updateUserGroups() throws SQLException {
        //clears the array and re-adds all of the groups
        //may add so it checks to see if they array contains the group instead
        this.userGroups.clear();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT group_id FROM group_user WHERE user_id = '" + getUserID() + "' AND status = 'y'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                this.userGroups.add(Group.returnGroupID(rs.getInt(1)));
            }
            con.close();
        } catch (ServletException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * A Method which adds the user to the users table
     *
     * @throws ServletException
     */
    public void persist() throws ServletException {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("INSERT INTO theuser "
                    + "(first_name,last_name,username,password,height,weight,bmi,dob,gender,email,targetweight,rating,lastLogin)"
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);");

            //ps.setInt(1, userID);
            ps.setString(1, firstName);
            ps.setString(2, lastName);
            ps.setString(3, username);
            ps.setString(4, password);
            ps.setDouble(5, height);
            ps.setDouble(6, weight);
            ps.setDouble(7, bmi);
            ps.setString(8, dob);
            ps.setString(9, gender);
            ps.setString(10, emailAddress);
            ps.setDouble(11, targetWeight);
            ps.setInt(12, rating);
            ps.setString(13, lastLogin);

            ps.executeUpdate();
            con.close();
        } catch (Exception e) {
            throw new ServletException("Persist Problem", e);
        }
    }

    /**
     * A method which returns a user given their email address. Returns all
     * information about that user to be used in various places across the
     * website.
     *
     * @param email the user email address
     * @return the user created in respect to the information retrieved.
     * @throws ServletException
     * @author john
     */
    public static User returnUser(String email) throws ServletException {
        User thisUser = new User();

        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM theuser WHERE email ='" + email + "'");

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                if (rs.getString("email").equals(email)) {
                    thisUser = new User(rs);
                    return thisUser;
                }
            }
            con.close();
        } catch (Exception e) {
            throw new ServletException("returning user problem", e);
        }

        return thisUser;
    }

    /**
     * A method which returns a user given their id. Returns all information
     * about that user to be used in various places across the website.
     *
     * @param userID the idea of the user we're trying to find
     * @return the user that has been found
     * @throws ServletException
     * @author John
     */
    public static User returnUserID(int userID) throws ServletException {
        User thisUser = new User();

        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM theuser WHERE user_id ='" + userID + "'");

            ResultSet rs = ps.executeQuery();
            con.close();

            if (rs.next()) {
                if (rs.getInt("user_id") == userID) {
                    thisUser = new User(rs);
                    return thisUser;
                }
            }
        } catch (Exception e) {
            throw new ServletException("returning user problem", e);
        }
        return thisUser;
    }

    /**
     * A Method which checks to see if a user with the same email address exists
     * This should stop any errors from happening when SQL tries adding
     * duplicates to the server.
     *
     * @return
     * @throws ServletException
     * @author John
     */
    public boolean check() throws ServletException {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement emailCheck = con.prepareCall("SELECT * FROM theuser WHERE email ='" + emailAddress + "'");
            ResultSet rs = emailCheck.executeQuery();
            con.close();
            if (rs.next()) {
                if (rs.getString("email").equals(emailAddress)) {
                    return false;
                }
            }
        } catch (Exception e) {
            throw new ServletException("Persist Problem", e);
        }
        return true;
    }

    public void update() throws SQLException, ClassNotFoundException {
        Connection con = DBAccess.getConnection();
        PreparedStatement ps1 = con.prepareStatement("update theuser set weight = '" + weight + "' where email ='" + emailAddress + "'");
        PreparedStatement ps2 = con.prepareStatement("update theuser set height = '" + height + "' where email ='" + emailAddress + "'");
        ps1.executeUpdate();
        ps2.executeUpdate();
        con.close();
    }
    public void updatehistory(String date) throws ClassNotFoundException, SQLException, ServletException {

        weightHistory.add(weight);

        heightHistory.add(height);
        bmiHistory.add(bmi);
        Connection con = DBAccess.getConnection();
        PreparedStatement ps = con.prepareStatement("INSERT INTO healthhistory "
                + "(theuseruser_id,weight,height,bmi,healthdate)"
                + "VALUES(?,?,?,?,?);");

        PreparedStatement psTwo = con.prepareStatement("SELECT MAX(user_goal_id)+1 FROM user_goal");
        ResultSet rs = psTwo.executeQuery();
        int maxValue = 0;
        if (rs.next()) {
            maxValue = rs.getInt(1);
        }
        ps.setInt(1, maxValue);

        //PreparedStatement psTwo = con.prepareStatement("SELECT MAX(user_goal_id)+1 FROM user_goal");
        //ResultSet rs = psTwo.executeQuery();
        //int maxValue = 0;
        //if (rs.next()) {
        //    maxValue = rs.getInt(1);
        //}
        ps.setInt(1, getUserID() );
        ps.setDouble(2, weight);
        ps.setDouble(3, height);
        ps.setDouble(4, bmi);
        ps.setString(5, date);
        ps.executeUpdate();

    }


    public void updateLastLogin(String log) throws SQLException, ClassNotFoundException, ServletException {

        Connection con = DBAccess.getConnection();
        PreparedStatement ps1 = con.prepareStatement("UPDATE theuser SET lastLogin ='" + log + "' where user_id =" + getUserID());
        ps1.executeUpdate();
        con.close();

    }

    public void weightGoalUpdate(WeightGoal w) throws ClassNotFoundException, SQLException, ServletException {
        Wgoal.add(w);
        Connection con = DBAccess.getConnection();
        PreparedStatement ps = con.prepareStatement("INSERT INTO user_goal "
                + "(user_goal_id,goal_name,targetweight,targetdate,status,completion_date,user_id)"
                + "VALUES(?,?,?,?,?,?,?);");
        PreparedStatement psTwo = con.prepareStatement("SELECT MAX(user_goal_id)+1 FROM user_goal");
        ResultSet rs = psTwo.executeQuery();
        int maxValue = 0;
        if (rs.next()) {
            maxValue = rs.getInt(1);
        }
        ps.setInt(1, maxValue);
        ps.setString(2, w.getName());
        ps.setDouble(3, w.getTarget());
        ps.setString(4, w.getTargetDate());
        ps.setString(5, Boolean.toString(w.isGoalAchieved()));
        ps.setString(6, "");
        ps.setInt(7, getUserID());
        ps.executeUpdate();
        con.close();
    }

    public void exerciseGoalUpdate(ExerciseGoal e) throws ClassNotFoundException, SQLException, ServletException {
        Egoal.add(e);
        Connection con = DBAccess.getConnection();
        PreparedStatement ps = con.prepareStatement("INSERT INTO exercise "
                + "(user_id, exercise_id,exercisename,typeofexercise,distance,duration,targettime,completiontime,targetdate,completiondate,status)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?);");
        PreparedStatement psTwo = con.prepareStatement("SELECT MAX(exercise_id)+1 FROM exercise");
        ResultSet rs = psTwo.executeQuery();
        int maxValue = 0;
        if (rs.next()) {
            maxValue = rs.getInt(1);
        }
        ps.setInt(1, getUserID());
        ps.setInt(2, maxValue);
        ps.setString(3, e.getName());
        ps.setString(4, e.getType());
        ps.setDouble(5, e.getDistance());
        ps.setDouble(6, e.getDuration());
        ps.setDouble(7, e.getTime());
        ps.setDouble(8, e.getCompletionTime());
        ps.setString(9, e.getTargetDate());
        ps.setString(10, "");
        ps.setString(11, Boolean.toString(e.isGoalAchieved()));
        ps.executeUpdate();
        con.close();
    }

    /**
     * Method used to update a food for the
     *
     * @param w
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void foodUpdate(Food f) throws ClassNotFoundException, SQLException {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("INSERT INTO food "
                    + "(foodid,food_name,calories,theuseruser_id)"
                    + "VALUES(?,?,?,?);");
            PreparedStatement psTwo = con.prepareStatement("SELECT MAX(foodid)+1 FROM food");
            ResultSet rs = psTwo.executeQuery();
            int maxValue = 0;
            if (rs.next()) {
                maxValue = rs.getInt(1);
            }
            ps.setInt(1, maxValue);
            ps.setString(2, f.getName());
            ps.setDouble(3, f.getCalories());
            ps.setInt(4, getUserID());

            ps.executeUpdate();
            con.close();
        } catch (ServletException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isInGroup(int groupID) {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT user_id FROM group_user where group_id='"
                    + groupID + "' and status ='y'");
            ResultSet set = ps.executeQuery();
            while (set.next()) {
                if (set.getInt(1) == getUserID()) {
                    return true;
                }
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void dietCaptureUpdate(DietCapture dc) throws ClassNotFoundException, SQLException {
        try {
            mealHistory.add(dc);
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("INSERT INTO mealCapture "
                    + "(theuseruser_id,mealid,mealname,food_one,drink_one,calories)"
                    + "VALUES(?,?,?,?,?,?);");
            PreparedStatement psTwo = con.prepareStatement("SELECT MAX(mealid)+1 FROM mealCapture");
            ResultSet rs = psTwo.executeQuery();
            int maxValue = 0;
            if (rs.next()) {
                maxValue = rs.getInt(1);
            }
            ps.setInt(1, getUserID());
            ps.setInt(2, maxValue);
            ps.setString(3, dc.getName());
            ps.setString(4, dc.getF());
            ps.setString(5, dc.getD());
            ps.setDouble(6, (dc.getTotalCals()));
            ps.executeUpdate();
            con.close();
        } catch (ServletException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Method that updates the users food list
     *
     * @throws SQLException
     */
    public void updateUsersFoods() throws SQLException, ServletException {
        this.foodList.clear();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from food where theuseruser_id = '" + userID + "';");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                this.foodList = (Food.returnAll(userID));
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param d
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public void drinkUpdate(Drink d) throws ClassNotFoundException, SQLException {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("INSERT INTO drink "
                    + "(drinkid,drink_name,calories,theuseruser_id)"
                    + "VALUES(?,?,?,?);");
            PreparedStatement psTwo = con.prepareStatement("SELECT MAX(drinkid)+1 FROM drink");
            ResultSet rs = psTwo.executeQuery();
            int maxValue = 0;
            if (rs.next()) {
                maxValue = rs.getInt(1);
            }
            ps.setInt(1, maxValue);
            ps.setString(2, d.getName());
            ps.setDouble(3, d.getCalories());
            ps.setInt(4, getUserID());
            ps.executeUpdate();
            con.close();
        } catch (ServletException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
