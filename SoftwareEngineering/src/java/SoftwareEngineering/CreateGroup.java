package SoftwareEngineering;

/**
 * A servlet which is used to add groups to the database
 */
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "CreateGroup", urlPatterns = {"/CreateGroup"})
public class CreateGroup extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String groupName = request.getParameter("groupName");
        String groupType = request.getParameter("groupType");
        //adds the new group from the form inputs obtained above
        Group newGroup = new Group(groupName, groupType, 0);

        HttpSession session = request.getSession();
        User thisUser = (User) session.getAttribute("userSession");

        if (newGroup.check() == true) {
            try {
                newGroup.persist();
                //adds the user and the group they are in to the table
                Group.updateUserAndGroup(newGroup, thisUser, "y");
                thisUser.updateUserGroups();
                request.getRequestDispatcher("group_display?groupName=" + groupName).forward(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(CreateGroup.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            request.getRequestDispatcher("invalid.jsp").forward(request, response);
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
