package SoftwareEngineering;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class setGoalController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        try {
            //  used to edit the user profile.
            HttpSession session = request.getSession(true);
            User user = (User) (session.getAttribute("userSession"));
            String day, month, year, targetDate;

            String nameW = request.getParameter("weightName");
            
            Double target = Double.parseDouble(request.getParameter("Weight"));
            day = request.getParameter("DayW");
            month = request.getParameter("MonthW");
            year = request.getParameter("YearW");
            targetDate = "" + day + "/" + month + "/" + year + "";
            
            WeightGoal wg = new WeightGoal(nameW,target,targetDate);
            user.weightGoalUpdate(wg);
            
            session.setAttribute("userSession", user);
            response.sendRedirect("ProfilePage.jsp");
        }
        catch(IOException e){
            System.out.println("Error");
        } catch (NumberFormatException e) {
            System.out.println("Error");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(setGoalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(setGoalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
