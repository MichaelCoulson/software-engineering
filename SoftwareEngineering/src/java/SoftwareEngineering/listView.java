package SoftwareEngineering;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Will
 */
public class listView extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String query = request.getParameter("searchQ");
            String type = request.getParameter("type");
            ArrayList groups = Group.returnAll(query, type);
            request.setAttribute("groupList", groups);
            request.getRequestDispatcher("search.jsp").forward(request, response);
            
        } catch (SQLException ex) {
            Logger.getLogger(listView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(listView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
