/**
 * This class is going to be used to create a food object that will store the amount
 * of calories and energy etc a food contains it could also hold an arraylsit of 
 * all the different drinks if we didnt want to use a database but that would 
 * probably be counter productive.
 */
package SoftwareEngineering;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

/**
 *
 * @author michaelcoulson
 */
public class Food {
    
    String name;
    double calories;
    static ResultSet rs = null;
    
    //Constructor
    public Food(String name, double calories){
    
        this.name = name;
        this.calories = calories;
    }
    
    public Food(){
    
    }
    
        public Food(ResultSet rs) throws SQLException {
        this.name = rs.getString("food_name");
        this.calories = rs.getDouble("calories");
    }

  /**
   * set anme.
   * @param name 
   */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * set calories
     * @param calories 
     */
    public void setCalories(double calories) {
        this.calories = calories;
    }

    /**
     * get name
     * @return 
     */
    public String getName() {
        return name;
    }

    /**
     * get calories
     * @return 
     */
    public double getCalories() {
        return calories;
    }
    


     public static ArrayList returnAll(int search) throws ServletException, SQLException, ClassNotFoundException
    {
        ArrayList foodList = new ArrayList();
            String searchQuery;
        
                searchQuery = "select * from food where theuseruser_id = '" + search + "';";
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement(searchQuery);
            rs = ps.executeQuery();
            while (rs.next()) 
            {
                Food f = new Food(rs);
                foodList.add(f);
            }
        return foodList;
    }
     

}
