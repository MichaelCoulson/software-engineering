/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SoftwareEngineering;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        String email = request.getParameter("Email");
        String password = request.getParameter("Password");
        HttpSession session = request.getSession(true);
        if (tryLogin(email, password) == true) {
            User thisUser = User.returnUser(email);
            session.setMaxInactiveInterval(30 * 60);
            
            int userID = thisUser.getUserID();
            
            Date d = new Date();
            SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
            
            if(dt1.format(d).compareTo(thisUser.getLastLogin()) == 1){
                thisUser.updateLastLogin(dt1.format(d));
                session.setAttribute("userSession", thisUser);
                request.getRequestDispatcher("settings.jsp").forward(request, response);
            }else{
                session.setAttribute("userSession", thisUser);
                request.getRequestDispatcher("ProfilePage.jsp").forward(request, response);
            }
            
        } else {
            //Prints an Error Message on the Index Page if Login is Unsucuessful
            request.setAttribute("message", "Invalid Email Or Password Please Try Again!");
            request.getRequestDispatcher("index.jsp").forward(request, response);
            //response.sendRedirect("index.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * A Method which checks to see if the users credentials are correct
     *
     * @param email the users inputted email address
     * @param password the users inputted password
     * @return true if the credentials are correct false if the credentials are
     * incorrect.
     * @throws ServletException
     */
    public boolean tryLogin(String email, String password) throws ServletException {
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement passwordcheck = con.prepareStatement("SELECT * FROM theuser WHERE email='" + email + "'");
            ResultSet rs = passwordcheck.executeQuery();
            con.close();
            if (rs.next()) {
                if (rs.getString("email").equals(email)) {
                    if (rs.getString("password").equals(password)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            throw new ServletException("Persist Problem", e);
        }
        return false;
    }
}
