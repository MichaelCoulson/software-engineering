/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SoftwareEngineering;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rah12nyu
 */
public class addFoodController extends HttpServlet {

   @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        try {
            //  used to edit the user profile.
            HttpSession session = request.getSession(true);
            User user = (User) (session.getAttribute("userSession"));
            
            String foodName = request.getParameter("foodName");
            Double calories = Double.parseDouble(request.getParameter("calories"));
            
            Food nf = new Food(foodName,calories);
            user.foodUpdate(nf);
            
            session.setAttribute("userSession", user);
            response.sendRedirect("ProfilePage.jsp");
        }
        catch(IOException e){
            System.out.println("Error");
            //Prints an Error Message on the Index Page if Login is Unsucuessful
            request.setAttribute("message", "Invalid Email Or Password Please Try Again!");
            request.getRequestDispatcher("ProfilePage.jsp").forward(request,response);
            //response.sendRedirect("index.jsp");
        } catch (NumberFormatException e) {
            System.out.println("Error");
            response.sendRedirect("ProfilePage.jsp");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(setGoalController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(setGoalController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
