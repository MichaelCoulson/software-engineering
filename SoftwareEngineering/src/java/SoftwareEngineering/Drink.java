/**
 * This class is going to be used to create a drink that will stores the amount
 * of calories and energy etc a drink contains it could also hold an arraylsit of 
 * all the different drinks if we didnt want to use a database but that would 
 * probably be counter productive.
 */
package SoftwareEngineering;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;

/**
 *
 * @author michaelcoulson
 */
public class Drink {
    
    private String name;
    private double calories;
    static ResultSet rs = null;
    
    //Constructor
    public Drink(String name, double calories){
    
        this.name = name;
        this.calories = calories;
    } 
    
      public Drink(){
    
    }
    
    public Drink(ResultSet rs) throws SQLException {
        this.name = rs.getString("drink_name");
        this.calories = rs.getDouble("calories");
    }
           
  

    /**
     * gets the name
     * @return 
     */
    public String getName() {
        return name;
    }

    /**
     * gets the calories
     * @return 
     */
    public double getCalories() {
        return calories;
    }

    /**
     * sets the name
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * sets the calories
     * @param calories 
     */
    public void setCalories(double calories) {
        this.calories = calories;
    }

         public static ArrayList returnAll(int search) throws ServletException, SQLException, ClassNotFoundException
    {
            ArrayList drinkList = new ArrayList();
            String searchQuery;
        
            searchQuery = "select * from drink where theuseruser_id = '" + search + "';";
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement(searchQuery);
            rs = ps.executeQuery();
            while (rs.next()) 
            {
                Drink d = new Drink(rs);
                drinkList.add(d);
            }
        return drinkList;
    }
    
}
