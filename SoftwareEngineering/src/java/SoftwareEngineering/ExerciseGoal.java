/**
 * This class will inherit from the goal class.
 */
package SoftwareEngineering;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bradley
 */
public class ExerciseGoal {

    private String name;
    private String targetDate;
    private String type;
    private double distance;
    private double duration;
    private double time;
    private double completionTime;
    private String completionDate;
    private boolean goalAchieved;
    private int exerciseID;

    public ExerciseGoal() {

    }

    public ExerciseGoal(ResultSet rs) throws SQLException {
        this.exerciseID = rs.getInt("exercise_id");
        this.name = rs.getString("exercisename");
        this.targetDate = rs.getString("targetdate");
        this.type = rs.getString("typeofexercise");
        this.distance = rs.getInt("distance");
        this.duration = Double.parseDouble(rs.getString("duration"));
        this.time = rs.getDouble("targettime");
        this.goalAchieved = rs.getString("status").equals("true");
        this.completionTime = Double.parseDouble(rs.getString("completiontime"));
        this.completionDate = rs.getString("completiondate");
    }

    public ExerciseGoal(String name, String targetDate, String type, double distance, double duration, double time) {
        this.name = name;
        this.targetDate = targetDate;
        this.type = type;
        this.distance = distance;
        this.duration = duration;
        this.time = time;
        this.goalAchieved = false;
    }
    
    public int getExerciseID(){
        return exerciseID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(String targetDate) {
        this.targetDate = targetDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public double getCompletionTime() {
        return completionTime;
    }

    public void setCompletionTime(double completionTime) {
        this.completionTime = completionTime;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public boolean isGoalAchieved() {
        return goalAchieved;
    }

    //make this auto update
    public void setGoalAchieved(boolean goalAchieved) {
        this.goalAchieved = goalAchieved;
    }
        
    /**
     * A Method which gets all the information about a given exercise goal.
     * @param id the id of the goal trying to achieve.
     * @return the exercise generated from the given id
     */
    public static ExerciseGoal getExerciseGoal(int id) {
        ExerciseGoal thisGoal = new ExerciseGoal();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM exercise WHERE exercise_id ='" + id + "'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                thisGoal = new ExerciseGoal(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ExerciseGoal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ExerciseGoal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return thisGoal;
    }
}
