package SoftwareEngineering;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ptj12sdu
 */
public class setGoalControllerExercise extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            //  used to edit the user profile.
            HttpSession session = request.getSession(true);
            User user = (User) (session.getAttribute("userSession"));
            String day, month, year, targetDate;
            double dur = 0;
            double dist = Double.parseDouble(request.getParameter("Distance"));
            String nameE = request.getParameter("exerciseName");    
            System.out.println(request.getParameter("targetTime"));
            double time = Double.parseDouble(request.getParameter("targetTime"));

            String type = request.getParameter("Exercise");

            if (request.getParameter("DayE") != null) {
                dur = Double.parseDouble(request.getParameter("Duration"));

                day = request.getParameter("DayE");
                month = request.getParameter("MonthE");
                year = request.getParameter("YearE");
                targetDate = day + "/" + month + "/" + year;
            } else {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -7);
                SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
                targetDate = format1.format(cal.getTime());
                dist = dist + ((dist / 100) * 20);            
            }

            ExerciseGoal eg = new ExerciseGoal(nameE, targetDate, type, dist, dur, time);
            user.exerciseGoalUpdate(eg);
            session.setAttribute("userSession", user);
            response.sendRedirect("ProfilePage.jsp");
        } catch (IOException e) {
            System.out.println("Error IO");
        } catch (NumberFormatException e) {
            System.out.println("Error number");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(setGoalControllerExercise.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(setGoalControllerExercise.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
}
