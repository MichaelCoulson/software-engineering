package SoftwareEngineering;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;

public class GroupExerciseGoal {

    private double completionTime;
    private int goalID;
    private int groupID;
    private boolean status;
    private String groupName;
    private String completionDate;
    private String goalName;
    private String targetDate;
    private ArrayList<User> user;
    String type;
    double dist;
    double dur;
    double time;
    Group g;
    private boolean goalAchieved;

    public GroupExerciseGoal() {

    }

    public GroupExerciseGoal(String groupName, String goalName, String targetDate, String type, double dist, double dur, double time) {
        this.goalName = goalName;
        this.groupName = groupName;
        this.targetDate = targetDate;
        this.targetDate = targetDate;
        this.type = type;
        this.dist = dist;
        this.dur = dur;
        this.time = time;
    }

    public GroupExerciseGoal(ResultSet rs) throws SQLException {
        try {
            this.completionDate = rs.getString("completiondate");
            this.goalID = rs.getInt("exercise_id");
            this.groupID = rs.getInt("group_id");
            this.goalName = rs.getString("exercisename");
            this.groupName = Group.returnGroupID(rs.getInt("group_id")).getGroupName();;
            this.targetDate = rs.getString("targetdate");
            this.type = rs.getString("typeofexercise");
            this.dist = rs.getInt("distance");
            this.goalAchieved = rs.getString("status").equals("true");
            this.dur = Double.parseDouble(rs.getString("duration"));
            this.time = Double.parseDouble(rs.getString("targettime"));
        } catch (ServletException ex) {
            Logger.getLogger(GroupGoal.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public int getGoalID(){
        return this.goalID;
    }
    
    public String getGoalType() {
        return this.type;
    }

    public String getGoalName() {
        return this.goalName;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public String getTargetDate() {
        return this.targetDate;
    }

    public int getGroupID() {
        return this.groupID;
    }

    public String getType() {
        return this.type;
    }

    public double getGoalDistace() {
        return this.dist;
    }

    public double getGoalDuration() {
        return this.dur;
    }

    public double getGoalTime() {
        return this.time;
    }

    public double getGoalCompletionTime() {
        return this.completionTime;
    }

    public boolean isGoalAchieved() {
        return goalAchieved;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    /**
     * A Method which gets all the information about a given exercise goal.
     *
     * @param id the id of the goal trying to achieve.
     * @return the exercise generated from the given id
     */
    public static GroupExerciseGoal getExerciseGoal(int id) {
        GroupExerciseGoal thisGoal = new GroupExerciseGoal();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM groupexercise WHERE exercise_id ='" + id + "'");
            ResultSet rs = ps.executeQuery();
            con.close();
            while (rs.next()) {
                thisGoal = new GroupExerciseGoal(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ExerciseGoal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ExerciseGoal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return thisGoal;
    }

}
