/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//WHAT IS THIS? - JOHN
package SoftwareEngineering;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserDA {

    static Connection theCon = null;
    static ResultSet result = null;

    public static User login(User bean) {

        Statement thestmt = null;

        String Email = bean.getEmailAddress();
        String Password = bean.getPassword();
      
        String theQuery = "SELECT * FROM theuser WHERE email = '" + Email + "' AND password = '" + Password + "'";
        System.out.println("Query:" + theQuery);

        try {
            theCon = DBAccess.getConnection();
            thestmt = theCon.createStatement();
            result = thestmt.executeQuery(theQuery);
            boolean further = result.next();

            if (!further) {
                System.out.println("USER NOT VALID PLEASE SIGNUP");
                
                bean.setValid(false);
            } else if (further) {

                String firstName = result.getString("first_name");
                String lastName = result.getString("last_name");
                
                System.out.println("Welcome :" + firstName);
                bean.setFirstName(firstName);
                bean.setLastName(lastName);
                bean.setValid(true);
            }
        } catch (SQLException e) {
            System.out.println("Login Failed an Error has Occured" + e);

        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (Exception e) {
                    result = null;
                }

                if (thestmt != null) {
                    try {
                        thestmt.close();

                    } catch (SQLException e) {

                    }
                    thestmt = null;
                }
                if (theCon != null) {
                    try {
                        theCon.close();

                    } catch (SQLException e) {

                    }
                    theCon = null;
                }
            }
            return bean;
        }

    }

}
