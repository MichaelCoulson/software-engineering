package SoftwareEngineering;

/**
 * A servlet which is used to control comments users to a group
 * @author John
 */
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "commentController", urlPatterns = {"/commentController"})
public class commentController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        HttpSession session = request.getSession();
        
        String groupMessage = request.getParameter("groupComment");
        User thisUser = (User) session.getAttribute("userSession");
        Group thisGroup = (Group) session.getAttribute("groupSession");
        
        Group.addUserMessage(thisGroup, thisUser, groupMessage);
        response.sendRedirect("group_display?groupName="+thisGroup.getGroupName());

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
