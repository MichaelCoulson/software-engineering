package SoftwareEngineering;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * SignUpServlet.java is used to sign up a new user.
 *
 * @author michaelcoulson
 */
public class updateExerciseGoal extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(true);
        User user = (User) (session.getAttribute("userSession"));
        
        int goalID = Integer.parseInt(request.getParameter("groupID"));
        String time = request.getParameter("timeTaken");
        String day = request.getParameter("Day");
        String month = request.getParameter("Month");
        String year = request.getParameter("Year");
        String date = day + "/" + month + "/" + year;


        
        user.updateExerciseGoal(goalID, date, time);
        session.setAttribute("userSession", user);
        response.sendRedirect("ProfilePage.jsp");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
