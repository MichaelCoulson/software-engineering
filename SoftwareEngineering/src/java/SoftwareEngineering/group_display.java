/*
* A servlet which sets up the group display.
*/
package SoftwareEngineering;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "group_display", urlPatterns = {"/group_display"})
public class group_display extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        //gets the group name passed through the url
        String groupName = request.getParameter("groupName");
        //assigns a new group based on the group name
        Group thisGroup = Group.returnGroup(groupName);
        //sets a new aspect of the session to hold this groups info
        session.setAttribute("groupSession", thisGroup);      
        //forwards to the grouppage.
        response.sendRedirect("GroupPage.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
