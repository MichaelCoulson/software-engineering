package SoftwareEngineering;

import static SoftwareEngineering.Food.rs;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;

/**
 *
 * @author michaelcoulson
 */
public class HealthOverview {
    
    double height;
    double weight;
    double BMI;
    String date;
    //Not sure whether to include a user object here or whether to have a healthOverview object in the user??
    //John - Personally id keep everything within user. Is that what the diagram
    //          showed? Keeping everything in one place would help

  //  ArrayList<Goal> userGoals;
    
    public HealthOverview(double height, double weight, String date){
    
        this.height = height;
        this.weight = weight;
        this.BMI = calculateBMI(height,weight);
        this.date = date;
        //this.userGoals = new ArrayList<Goal>();
    }
    
    public HealthOverview(){
    
    }

    HealthOverview(ResultSet rs) throws SQLException {
 //To change body of generated methods, choose Tools | Templates.
        this.height = rs.getDouble("height");
        this.weight = rs.getDouble("weight");
        this.BMI = rs.getDouble("bmi");
        this.date = rs.getString("healthdate");
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
    
       
    /**
     * gets the height
     * @return 
     */
    public double getHeight() {
        return height;
    }
    
    public static double calculateBMI(double weight,double height){
        double answer = weight / height;
        answer = answer / height;
        return answer;  
    }
    
    /**
     * gets the weight
     * @return 
     */
    public double getWeight() {
        return weight;
    }

    /**
     * gets the bmi
     * @return 
     */
    public double getBMI() {
        return BMI;
    }

    /**
     * gets the user
     * @return 
     */
  

    //Returns the goal arraylist
    //public ArrayList<Goal> getUserGoals() {
    //    return userGoals;
    //}

    /**
     * Sets the height
     * @param height 
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * Sets the weight.
     * @param weight 
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * Sets the BMI AKA Calculates it using the height and weight.
     * @param BMI 
     */
    public void setBMI(double BMI) {
        this.BMI = BMI;
    }

    /**
     * Sets the user
     * @param user 
     */


    /**
     * Method to set the whole arraylist to another arraylist.
     * @param userGoals 
     */
   // public void setUserGoals(ArrayList<Goal> userGoals) {
   //     this.userGoals = userGoals;
  //  }
    
    /**
     * Method to add a goal to the goal list.
     * @param g 
     */
   // public void addGoal(Goal g){
   //     this.userGoals.add(g);
   // }
    
    //Possibly Needs to be a little differrent
   // public Goal getGoal(int i){
   //     return this.userGoals.get(i);
   // }
    
    /*
    Standard to string method probably wont need it.
    */
     public static ArrayList returnAllWeights(int search) throws ServletException, SQLException, ClassNotFoundException
    {
            ArrayList hh = new ArrayList();

      
            String searchQuery;
        
            searchQuery = "select * from healthhistory where theuseruser_id = '" + search + "';";
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement(searchQuery);
            rs = ps.executeQuery();

            if (rs.next()) {
                    HealthOverview ho = new HealthOverview(rs);
                    hh.add(ho);
                    System.out.println(ho.getWeight());
                
        }

        return hh;
    }
    
}
