package SoftwareEngineering;

/**
 * A servlet which is used to invite users to a group
 */
import java.io.IOException;
import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "inviteController", urlPatterns = {"/inviteController"})
public class inviteController extends HttpServlet {

    private String host;
    private String port;
    private String user;
    private String pass;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //all possible variables are held here.
        String userEmail = request.getParameter("inviteUser");
        String invitationResponse = request.getParameter("inviteResponse");
        String groupName = request.getParameter("inviteGroup");

        //information contained within the if statement process an invitation.
        if (userEmail != null) {
            HttpSession session = request.getSession();
            User inviteUser = User.returnUser(userEmail);
            Group inviteGroup = (Group) session.getAttribute("groupSession");
            String recipient = inviteUser.getEmailAddress();
            String subject = "You Have Recieved an invite!";
            String content = "Some content Here!";

            if (inviteUser.check() != true) {
                String resultMessage = "";

                try {
                    EmailUtility.sendEmail(host, port, user, pass, recipient, subject,
                            content);
                    resultMessage = "The e-mail was sent successfully";
                    session.setAttribute("invitationUserSession", inviteUser);
                    if (inviteUser.isInGroup(inviteGroup.getGroupID(inviteGroup.getGroupName())) == false) {
                        Group.updateUserAndGroup(inviteGroup, inviteUser, "p");
                        request.setAttribute("Emailmessage", "Invalid Email - No User! Try Again!");
                        response.sendRedirect("GroupPage.jsp");
                    } else {
                        request.setAttribute("Emailmessage", "Member is in the group already!");
                        request.getRequestDispatcher("GroupPage.jsp").forward(request, response);
                    }
                } catch (MessagingException ex) {
                    resultMessage = "There were an error: " + ex.getMessage();
                } catch (IOException ex) {
                    resultMessage = "There were an error: " + ex.getMessage();
                }
            } else {
                System.out.println("User does not exist!");
                request.setAttribute("Emailmessage", "Invalid Email - No User! Try Again!");
                request.getRequestDispatcher("GroupPage.jsp").forward(request, response);
            }
        }
        //if a response is null manages accepting/declining an invitation
        if (invitationResponse != null) {
            HttpSession session = request.getSession();
            User thisUser = (User) session.getAttribute("userSession");
            Group thisGroup = Group.returnGroup(groupName);
            if (invitationResponse.equals("true")) {
                Group.updateUserAndGroup(thisGroup, thisUser, "y");
                response.sendRedirect("group_display?groupName=" + thisGroup.getGroupName());
            } else {
                Group.updateUserAndGroup(thisGroup, thisUser, "n");
                response.sendRedirect("ProfilePage.jsp");
            }
        }

    }

    public void init() {
        // reads SMTP server setting from web.xml file
        ServletContext context = getServletContext();
        host = context.getInitParameter("host");
        port = context.getInitParameter("port");
        user = context.getInitParameter("user");
        pass = context.getInitParameter("pass");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
