package SoftwareEngineering;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * SignUpServlet.java is used to sign up a new user.
 *
 * @author michaelcoulson
 */
public class SignUpServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        // Make an instance of a new user
        try {
            //Gathers the new users information 
            double theheight;
            double theweight;
            double tg;
            double bmi = 0;
            String firstName = request.getParameter("FirstName");
            String surname = request.getParameter("LastName");
            String username = request.getParameter("userName");
            String password = request.getParameter("Password");
            String height = request.getParameter("Height");
            String heighti = request.getParameter("Height1");
            String weighti = request.getParameter("Weight1");
            String weight = request.getParameter("Weight");
            String day = request.getParameter("DOBDay");
            String month = request.getParameter("DOBMonth");
            String year = request.getParameter("DOBYear");
            String gender = request.getParameter("Gender");
            String email = request.getParameter("Email");
            String targetWeightMetric = request.getParameter("targetWeight");
            String targetWeight = request.getParameter("targetWeight1");

            String dateobirth = "'" + day + "/" + month + "/" + year + "'";
   
            
            if (!weight.isEmpty() && weighti.isEmpty()) {
                theweight = Double.parseDouble(weight);
            } else {
                theweight = Double.parseDouble(weighti) * 6.35029;
            }
            
            if (!height.isEmpty() && heighti.isEmpty()) {
                
                theheight = Double.parseDouble(height);
            } else {
                theheight = Double.parseDouble(heighti) / 3.2808;
            }
                        
            if (!targetWeightMetric.isEmpty() && targetWeight.isEmpty()) {
                tg = Double.parseDouble(targetWeightMetric);
            } else {
                tg = Double.parseDouble(targetWeight) * 6.35029;
            }
            Date d = new Date();
            SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");

            User thisUser = new User(username, firstName, surname,
                    password, theheight, theweight, dateobirth, gender, email, bmi, 0, tg, dt1.format(d));
            //Used to set and calculate the BMI.
            bmi = thisUser.calculateBMI(theweight, theheight);
            thisUser.setBmi(bmi);
            
            if (thisUser.check() == true) {
 
                thisUser.persist();
                // Set the signUp attribute within the request 
                request.setAttribute("userSession", thisUser);
                request.getRequestDispatcher("confirmation.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("EmailExists.jsp").forward(request, response);
            }
        } catch (IOException e) {
            request.getRequestDispatcher("SignUp.jsp").forward(request, response);
        } catch (ServletException p) {
            request.getRequestDispatcher("UsernameExists.jsp").forward(request, response);
        }catch(NumberFormatException m){
             request.getRequestDispatcher("Heightweighterror.jsp").forward(request, response);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
