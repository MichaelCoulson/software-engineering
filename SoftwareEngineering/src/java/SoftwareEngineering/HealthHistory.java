/**
 * This class is going to be used to create a food object that will store the amount
 * of calories and energy etc a food contains it could also hold an arraylsit of 
 * all the different drinks if we didnt want to use a database but that would 
 * probably be counter productive.
 */
package SoftwareEngineering;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

/**
 *
 * @author michaelcoulson
 */
public class HealthHistory {
    
    String theuseruser_id;
    double weight;
    double height;
    double bmi;
    String date;
    static ResultSet rs = null;

    public HealthHistory(String theuseruser_id, double weight, double height, double bmi, String date) {
        this.theuseruser_id = theuseruser_id;
        this.weight = weight;
        this.height = height;
        this.bmi = bmi;
        this.date = date;
    }

    public HealthHistory(ResultSet rs) throws SQLException {
        this.theuseruser_id = rs.getString("theuseruser_id");
        this.weight = rs.getDouble("weight");
        this.height = rs.getDouble("height");
        this.bmi = rs.getDouble("bmi");
        this.date = rs.getString("healthdate");
    }
    
    public String getTheuseruser_id() {
        return theuseruser_id;
    }

    public void setTheuseruser_id(String theuseruser_id) {
        this.theuseruser_id = theuseruser_id;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getBmi() {
        return bmi;
    }

    public void setBmi(double bmi) {
        this.bmi = bmi;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public static ResultSet getRs() {
        return rs;
    }

    public static void setRs(ResultSet rs) {
        HealthHistory.rs = rs;
    }
    
     public static ArrayList returnAll(int search) throws ServletException, SQLException, ClassNotFoundException
    {
        ArrayList hhlist = new ArrayList();
            String searchQuery;
        
            searchQuery = "select * from healthhistory where theuseruser_id = '" + search + "';";
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement(searchQuery);
            rs = ps.executeQuery();
            while (rs.next()) 
            {
                HealthHistory hh = new HealthHistory(rs);
                hhlist.add(hh);
            }
        return hhlist;
    }
     

}
