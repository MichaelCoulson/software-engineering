<%-- 
    Document   : newjsp
    Created on : Apr 20, 2015, 6:33:23 PM
    Author     : michaelcoulson
--%>

<%@page import="SoftwareEngineering.Food"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
                                            <h1>List of Foods</h1>
                <%
                List foodList = (List) request.getAttribute("foodList");
                Iterator it = foodList.iterator();
                while(it.hasNext())
                {
                Food f = (Food) it.next();
                %>
                <hr>
                <p><%= f.getName() %> <%= f.getCalories() %> </p>
                <%
                }
                %>
    </body>
</html>
