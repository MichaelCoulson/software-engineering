<%@ page language="java" 
         contentType="text/html; charset=windows-1256"
         pageEncoding="windows-1256"
         import="SoftwareEngineering.User"
         import="java.util.*"
         import="SoftwareEngineering.Group"
         import="SoftwareEngineering.WeightGoal"
         %>

<!DOCTYPE HTML>
<html>
<head>
<script type="text/javascript">
    <% User user = (User) (session.getAttribute("userSession"));%>
window.onload = function () {
	var chart = CanvasJS.Chart("chartContainer", {
		theme: "theme2",//theme1
		title:{
			text: "Basic Column Chart - CanvasJS"              
		},
		animationEnabled: false,   // change to true
		data: [              
		{
			// Change type to "bar", "splineArea", "area", "spline", "pie",etc.
			type: "column",
			dataPoints: [
				{ label: "apple",  y: <%= user.getWeight()%> },
				{ label: "orange", y: 15  },
				{ label: "banana", y: 25  },
				{ label: "mango",  y: 30  },
				{ label: "grape",  y: 28  }
			]
		}
		]
	});
	chart.render();
}
</script>
 <script type="text/javascript" src="javascript/canvasjs.min.js" defer></script>
</head>
<body>
    
<div id="chartContainer" style="height: 300px; width: 100%;"></div>
</body>
</html>