<!doctype html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html">
  <title>Health Tracker</title>
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
  <script src="javascript/javascript.js" type="text/javascript"></script>
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
</head>
<body>
  <div id="topbar">
  <a href="index.jsp">Health Tracker</a>
  </div>
  
  <div id="w">
    <div id="content" class="clearfix">
      <h1>Confirmation of Sign Up</h1>
      
      <section id="bio">
          <script language="javascript">
        function emailCurrentPage() {
            window.location.href = "mailto:?subject=" + document.title + "&body=" + escape(window.location.href);
        }
    </script>

    <a href="javascript:emailCurrentPage()">Mail this page!</a>




    <body>
        <jsp:useBean id="userSession" type="SoftwareEngineering.User" scope="request" /> 

                        <!--MAIN CONTENT-->	
                        <p>Congratulations <jsp:getProperty name= "userSession" property="firstName" /> you have successfully signed up to HealthTracker! 
                            This page is to confirm that your sign up has been completed!  </p>

                        <p>You will shortly receive a confirmation of your sign up by email. If you have not received an email you can send or 
                            print yourself a copy of this confirmation page by using the buttons below. <br /></p>

                        <p>To start using Health Tracker and find your friends please <a href="index.jsp">Click Here!</a><br /></p>

                        <p>Thank you. <a href="index.jsp">HealthTracker</a></p>
                        <hr />

                        <h3>Confirmation Summary</h3>

                        <!--TABLE OF DETAILS-->
                        <h4>Personal Details</h4>

                        <table id="table" >
                            <tr>
                                <td><strong>ID</strong></td>

                                <td><strong>First Name</strong></td>

                                <td><strong>Surname</strong></td>

                                <td><strong>Email Address</strong></td>

                                <td><strong>Year of Birth</strong></td>

                                <td><strong>Gender</strong></td>
                                
                                <td><strong>Height</strong></td>
                                
                                <td><strong>Weight</strong></td>
                                
                                <td><strong>BMI</strong></td>
                            </tr>

                            <tr>
                                <td><jsp:getProperty name= "userSession" property="userID" /></td>

                                <td><jsp:getProperty name= "userSession" property="firstName" /></td> 

                                <td><jsp:getProperty name= "userSession" property="lastName" /></td> 

                                <td><jsp:getProperty name= "userSession" property="emailAddress" /></td> 

                                <td><jsp:getProperty name= "userSession" property="dob" /></td> 

                                <td><jsp:getProperty name= "userSession" property="gender" /></td> 
                                
                                <td><jsp:getProperty name= "userSession" property="height" /></td> 

                                <td><jsp:getProperty name= "userSession" property="weight" /></td> 

                                <td><jsp:getProperty name= "userSession" property="bmi" /></td> 
                                
                            </tr>
                        </table>

                        <p>Click <a href="javascript:window.print()">HERE</a> to print this page</p>

                        <p>Click <a href="javascript:emailCurrentPage()">HERE</a> to email yourself this page</p>

      </section>
    </div><!-- @end #content -->
  </div><!-- @end #w -->
</body>
</html>
</html>

