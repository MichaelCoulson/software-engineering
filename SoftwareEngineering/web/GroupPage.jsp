
<%@page import="SoftwareEngineering.ExerciseGoal"%>
<%@page import="SoftwareEngineering.GroupExerciseGoal"%>
<%@page import="SoftwareEngineering.GroupGoal"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" 
         contentType="text/html; charset=windows-1256"
         pageEncoding="windows-1256"
         import="SoftwareEngineering.User"
         import="java.util.*"
         import="SoftwareEngineering.Group"
         import="SoftwareEngineering.Group.newMessage"
         import="SoftwareEngineering.WeightGoal"
         %>
<%
    response.setHeader("Expires", "Sun, 7 May 1995 12:00:00 GMT");
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
    response.setHeader("Pragma", "no-cache");

    User user = (User) session.getAttribute("userSession");
    Group thisGroup = (Group) session.getAttribute("groupSession");
    if (user == null || thisGroup == null) {
        response.sendRedirect("index.jsp");
    } else {
%>
<html>
    <head>
        <title>Health Tracker</title>
        <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
        <script src="javascript/javascript.js" type="text/javascript"></script>
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="topbar">
            <a style="float: left; padding-left: 10px;" href="Logout.jsp">[Log Out]</a>
            <a style="padding-left: 220px;" href="ProfilePage.jsp">Health Tracker</a>
            <form style="margin: 0; padding: 0; display: inline; float:right; padding-right: 10px;" action="listView" method="post">
                <input style="display: inline;" name="searchQ" type="text" class ="input" value ="Search for Group" />
                <select style="display: inline;" name="type" value="">
                    <option value="All">All Groups</option>
                    <option value="Diet Group">Diet Group</option>
                    <option value="Gentle Exercise Group">Gentle Exercise Group</option>
                    <option value="Extreme Exercise Group">Extreme Exercise Group</option>
                </select>
                <input style="display: inline;" type="submit" name="submit" value="searchGroup" class="button" />
            </form>  
        </div>

        <div id="w">
            <div id="content" class="clearfix">
                <h1><%= thisGroup.getGroupName()%></h1>
                <h2>Category: <%= thisGroup.getCatagory()%><br>Rating: <%= thisGroup.getRating()%></h2>

                <section id="bio">
                    <form name="invite_user" action="inviteController" method="post">
                        <input name="inviteUser" type="text" class ="input" required= "required"  value ="User Email address" />
                        <div class="footer">
                            <input type="submit" name="submit" value="Invite User" class="button" />
                        </div>
                    </form>  
                    ${Emailmessage}
                    <br>
                    <h2>The users in this group are:</h2>

                    <%
                        Iterator<User> iterator = thisGroup.getUsers().iterator();
                        while (iterator.hasNext()) {
                    %>

                    <ul id="friendslist" class="clearfix">
                        <li><a href="#"><img src="images/avatar.png" alt="" width="22" height="22"> <% out.println(iterator.next().getFirstName()); %></a> </li>        
                    </ul>
                    <%
                        }
                    %>

                    <h2>Comments For This Group Are</h2>


                    <table class="tftable">
                        <tr>
                            <th>User</th>
                            <th>Comment</th>
                            <!-- removed   <th>Add/remove/edit(will hide this)</th> -->
                        </tr>
                        <tr>
                            <%
                                Iterator<newMessage> messageIterator = thisGroup.getMessages().iterator();
                                while (messageIterator.hasNext()) {
                                    newMessage currentMessage = messageIterator.next();
                                    String poster = User.returnUserID(currentMessage.getMessagePoster()).getFirstName();
                                    String message = currentMessage.getMessage();
                            %>
                            <td style="width: 20%;"><% out.println(poster);%> says...</td>
                            <td><% out.println(message);%></td>   
                        </tr>
                        <%
                            }
                        %>
                    </table>
                    <form name="new_Comment" action="commentController" method="post">
                        <input name="groupComment" type="text" required= "required"  style="width: 100%;" value ="Enter your message here..." />
                        <div class="footer">
                            <input type="submit" name="submit" value="Add Comment" />
                        </div>
                    </form>  
                    <br>
                    <h2>Latest Goal Achievements For This Group</h2>
                    <table class="tftable">
                        <tr>
                            <th>User</th>
                            <th>Goal Achieved</th>
                            <th>Date Achieved</th>
                        </tr>
                        <tr>
                            <%
                                Iterator<User> iteratorUsers = thisGroup.getUsers().iterator();
                                while (iteratorUsers.hasNext()) {
                                    User thisWeightUser = iteratorUsers.next();
                                    Iterator<WeightGoal> weightGoalIterator = thisGroup.updateWeightGoals(thisWeightUser.getUserID()).iterator();
                                    Iterator<ExerciseGoal> exerciseGoalIterator = thisGroup.updateExerciseGoals(thisWeightUser.getUserID()).iterator();
                                    while (weightGoalIterator.hasNext()) {
                                        WeightGoal thisWeightGoal = weightGoalIterator.next();
                                        String weightUser = thisWeightUser.getFirstName();
                                        String goalWeightName = thisWeightGoal.getName();
                                        String goalWeightAchieved = thisWeightGoal.getCompletionDate();
                            %>
                            <td><% out.println(weightUser);%> Achieved their...</td>
                            <td><% out.println(goalWeightName);%> Goal!</td> 
                            <td><% out.println(goalWeightAchieved);%></td>
                        </tr>
                        <%
                            }
                            while (exerciseGoalIterator.hasNext()) {
                                ExerciseGoal thisExerciseGoal = exerciseGoalIterator.next();
                                String exerciseUser = thisWeightUser.getFirstName();
                                String goalExercisetName = thisExerciseGoal.getName();
                                String goalExerciseAchieved = thisExerciseGoal.getCompletionDate();
                        %>
                        <td><% out.println(exerciseUser);%> Achieved their...</td>
                        <td><% out.println(goalExercisetName);%> Goal!</td> 
                        <td><% out.println(goalExerciseAchieved);%></td>
                        </tr>
                        <%
                                }
                            }
                        %>
                    </table>

                    <h2>Weight Goals</h2>

                    <table class="tftable">
                        <tr>
                            <th>Goal Name</th>
                            <th>Target Date</th>
                            <th>Target Weight</th>
                            <th>Completion Date</th>
                            <th>Status</th>
                        </tr>
                        <tr>
                            <%
                                thisGroup.updateWeightGoals();
                                Iterator<GroupGoal> groupWeightIterator = thisGroup.getGroupWeightGoals().iterator();
                                while (groupWeightIterator.hasNext()) {
                                    SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
                                    GroupGoal thisWeightGoal = groupWeightIterator.next();
                                    String goalName = thisWeightGoal.getGoalName();
                                    String tar = thisWeightGoal.getTargetDate();
                                    boolean achieved = thisWeightGoal.isGoalAchieved();
                                    String comp = thisWeightGoal.getCompletionDate();
                                    double targetWeight = thisWeightGoal.getWeightGoal();
                                    Date targetDate = formatDate.parse(tar);
                                    Date today = new Date();
                            %>
                            <td><% out.println(goalName);%></td>
                            <td><% out.println(tar);%></td> 
                            <td><% out.println(targetWeight);%></td>
                            <td><%out.println(comp);%></td> 
                            <td><% if (today.after(targetDate) && (achieved == false)) {
                                    out.println("Expired");
                                } else if (today.before(targetDate) && (achieved == true)) {
                                    out.println("Achieved!");
                                } else {
                                    out.println("Current");
                                } %></td>
                        </tr>
                        <%
                            }
                        %>
                    </table>


                    <h2>Exercise Goals:</h2>



                    <table class="tftable">
                        <tr>
                            <th>Goal Name</th>
                            <th>Target Date</th>
                            <th>Goal Type</th>
                            <th>Distance Remaining</th>
                            <th>Completion Date</th>
                            <th>Completed?</th>
                        </tr>
                        <tr>
                            <%
                                thisGroup.updateExerciseGoals();
                                Iterator<GroupExerciseGoal> goalExerciseIterator = thisGroup.getEgoal().iterator();

                                while (goalExerciseIterator.hasNext()) {
                                    GroupExerciseGoal thisGoal = goalExerciseIterator.next();
                                    String goalName = thisGoal.getGroupName();
                                    String tar = thisGoal.getTargetDate();
                                    String comp = thisGoal.getCompletionDate();
                                    String type = thisGoal.getType();
                                    double timeTaken = thisGoal.getGoalCompletionTime();
                                    double distance = thisGoal.getGoalDistace();
                            %>
                        <tr>
                            <td valign="center"><% out.println(goalName);%></td>
                            <td valign="center"><% out.println(tar);%></td> 
                            <td valign="center"><% out.println(type);%></td>
                            <td valign="center"><% out.println(distance); %></td>
                            <% if (thisGoal.isGoalAchieved() == true) {%>
                            <td valign="center"><%out.println(comp);%></td>
                            <td valign="center"><%out.println("Completed!");%></td>
                        </tr>
                        <% } else {
                        %>
                        <td colspan="2">
                            <form id ="f1" name="input" action="updateGroupExerciseGoal" method="post">
                                <input type="hidden" name="groupID" value="<%=thisGoal.getGoalID()%>">
                                Distance Achieved: <input type="text" name="distanceMoved" style="width: 50px;" />
                                <input type="submit" name="submit" value="Update!" class="button" />
                            </form>
                        </td>
                        </tr>
                        <%
                                }
                            }
                        %>

                    </table>
                </section>

                <form id ="f1" name="input" action="setGroupGoalController" method="post">
                    <fieldset>

                        <br>
                        <legend>Set Weight Goal:</legend>

                        <br>
                        Goal Name <br>
                        <input id="input2" type="text" name="weightName" size="30" value=""><br>        
                        <br>

                        Target Weight Loss/Gain <br>
                        <select name="weightGoal" value="">
<<<<<<< HEAD
                            <option> - Weight - </option>
                            <option value="3000">3000 kg</option>
                            <option value="2900">2900 kg</option>
                            <option value="2800">2800 kg</option>
                            <option value="2700">2700 kg</option>
                            <option value="2600">2600 kg</option>
                            <option value="2500">2500 kg</option>
                            <option value="2400">2400 kg</option>
                            <option value="2300">2300 kg</option>
                            <option value="2200">2200 kg</option>
                            <option value="2100">2100 kg</option>
                            <option value="2000">2000 kg</option>
                            <option value="1900">1900 kg</option>
                            <option value="1800">1800 kg</option>
                            <option value="1700">1700 kg</option>
                            <option value="1600">1600 kg</option>
                            <option value="1500">1500 kg</option>
                            <option value="1400">1400 kg</option>
                            <option value="1300">1300 kg</option>
                            <option value="1200">1200 kg</option>
                            <option value="1100">1100 kg</option>
                            <option value="1000">1000 kg</option>
                            <option value="900">900 kg</option>
                            <option value="800">800 kg</option>
                            <option value="700">700 kg</option>
                            <option value="600">600 kg</option>
                            <option value="500">500 kg</option>
                            <option value="400">400 kg</option>
                            <option value="300">300 kg</option>
                            <option value="290">290 kg</option>
                            <option value="280">280 kg</option>
                            <option value="270">270 kg</option>
                            <option value="260">260 kg</option>
                            <option value="250">250 kg</option>
                            <option value="240">240 kg</option>
                            <option value="230">230 kg</option>
                            <option value="220">220 kg</option>
                            <option value="210">210 kg</option>
                            <option value="200">200 kg</option>
                            <option value="190">190 kg</option>
                            <option value="180">180 kg</option>
                            <option value="170">170 kg</option>
                            <option value="160">160 kg</option>
                            <option value="150">150 kg</option>
                            <option value="140">140 kg</option>
                            <option value="130">130 kg</option>
                            <option value="120">120 kg</option>
                            <option value="110">110 kg</option>
                            <option value="100">100 kg</option>
                            <option value="90">90 kg</option>
                            <option value="80">80 kg</option>
                            <option value="70">70 kg</option>
                            <option value="60">60 kg</option>
                            <option value="50">50 kg</option>
                            <option value="40">40 kg</option>
=======
                            <option value="" selected="selected"> - Weight - </option>
                            <option value="0.1">100 g</option>
                            <option value="0.2">200 g</option>
                            <option value="0.3">300 g</option>
                            <option value="0.4">400 g</option>
                            <option value="0.5">500 g</option>
                            <option value="0.6">600 g</option>
                            <option value="0.7">700 g</option>
                            <option value="0.8">800 g</option>
                            <option value="0.9">900 g</option>
                            <option value="1">1.0 kg</option>
                            <option value="1.1">1.1 kg</option>
                            <option value="1.2">1.2 kg</option>
                            <option value="1.3">1.3 kg</option>
                            <option value="1.4">1.4 kg</option>
                            <option value="1.5">1.5 kg</option>
                            <option value="1.6">1.6 kg</option>
                            <option value="1.7">1.7 kg</option>
                            <option value="1.8">1.8 kg</option>
                            <option value="1.9">1.9 kg</option>
                            <option value="2">2.0 kg</option>
                            <option value="2.1">2.1 kg</option>
                            <option value="2.2">2.2 kg</option>
                            <option value="2.3">2.3 kg</option>
                            <option value="2.4">2.4 kg</option>
                            <option value="2.5">2.5 kg</option>
                            <option value="2.6">2.6 kg</option>
                            <option value="2.7">2.7 kg</option>
                            <option value="2.8">2.8 kg</option>
                            <option value="2.9">2.9 kg</option>
                            <option value="3.0">3.0 kg</option>
                            <option value="3.1">3.1 kg</option>
                            <option value="3.2">3.2 kg</option>
                            <option value="3.3">3.3 kg</option>
                            <option value="3.4">3.4 kg</option>
                            <option value="3.5">3.5 kg</option>
                            <option value="3.6">3.6 kg</option>
                            <option value="3.7">3.7 kg</option>
                            <option value="3.8">3.8 kg</option>
                            <option value="3.9">3.9 kg</option>
                            <option value="4">4.0 kg</option>
                            <option value="4.1">4.1 kg</option>
                            <option value="4.2">4.2 kg</option>
                            <option value="4.3">4.3 kg</option>
                            <option value="4.4">4.4 kg</option>
                            <option value="4.5">4.5 kg</option>
                            <option value="4.6">4.6 kg</option>
                            <option value="4.7">4.7 kg</option>
                            <option value="4.8">4.8 kg</option>
                            <option value="4.9">4.9 kg</option>
                            <option value="5">5.0 kg</option>
>>>>>>> e03829d9da801dd79c85ca62a5c0d76ef42b95a3
                        </select>


                        </select>
                        <br>
                        Target Date <br>
                        <select name="DayW" value="">
                            <option value="" selected="selected"> - Day - </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select>
                        <select name="MonthW" value="">
                            <option value="" selected="selected"> - Month - </option>
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        <select name="YearW" value="">
                            <option value="" selected="selected"> - Year - </option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>

                        </select>
                        <br>
                        <br>
                        <input id="input7" name="setGroupGoalController" type="submit" value="Set Goal!">
                    </fieldset>
                </form>

                <form id ="f1" name="input" action="setGroupGoalControllerExercise" method="post">
                    <fieldset>

                        <br>
                        <legend>Set Exercise: </legend>
                        <br>
                        Exercise Name <br>
                        <input id="input2" type="text" name="exerciseName" required= "required"  size="30" value=""><br>        
                        <br>
                        <select name="Exercise" value="">
                            <option value="" selected="selected"> - Exercise - </option>
                            <option value="walk">Walk</option>
                            <option value="run">Run</option>
                            <option value="cycling">Cycling</option>
                            <option value="rowing">Rowing</option>
                            <option value="swimming">Swimming</option>
                        </select><br>

                        <select name="Distance" value="">
                            <option value="" selected="selected"> - Distance - </option>
                            <option value="100">100 m</option>
                            <option value="200">200 m</option>
                            <option value="400">300</option>
                            <option value="400">400</option>
                            <option value="500">500m</option>
                            <option value="600">600m</option>
                            <option value="700">700m</option>
                            <option value="800">800m</option>
                            <option value="900">900m</option>
                            <option value="1000">1000m</option>
                            <option value="1500">1500m</option>
                            <option value="2500">2500m</option>
                            <option value="21097">Half Marathon</option>
                            <option value="42195">Marathon</option>
                        </select>
                        <br>

                        <br>
                        Target duration <br>
                        <input id="input222" type="number" min="0" name="Duration" size="30" value=""><br>        
                        <br>

                        Target time <br>
                        <input id="input2" type="number" min="0" name="targetTime" size="30" value=""><br>        
                        <br>
                        Target Date <br>
                        <select name="DayE" value="">
                            <option value="" selected="selected"> - Day - </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select>
                        <select name="MonthE" value="">
                            <option value="" selected="selected"> - Month - </option>
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        <select name="YearE" value="">
                            <option value="" selected="selected"> - Year - </option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                        </select>
                        <br>
                        <br>

                        <input id="input7" name="setGroupGoalControllerExercise" type="submit" value="Set Goal!">

                    </fieldset>
                </form>
            </div>
        </div>
        <%
            }
        %>
    </body>
</html>

