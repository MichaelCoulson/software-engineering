<%-- 
    Document   : invitationConfirmation
    Created on : 16-Apr-2015, 15:22:26
    Author     : John
--%>
<%@ page language="java" 
         contentType="text/html; charset=windows-1256"
         pageEncoding="windows-1256"
         import="SoftwareEngineering.User"
         import="java.util.*"
         import="SoftwareEngineering.Group"
         %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% User invitedUser = (User) (session.getAttribute("invitationUserSession"));%>
        <% Group invitedGroup = (Group) session.getAttribute("groupSession");%>
        <h1>Confirmation that you have sent an invite to <%= invitedUser.getFirstName()%> to join the <%= invitedGroup.getGroupName()%> Group</h1>
        <a href="ProfilePage.jsp">Back To Profile Page</a>
    </body>
</html>
