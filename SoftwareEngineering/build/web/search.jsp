<%@ page language="java" 
         contentType="text/html; charset=windows-1256"
         pageEncoding="windows-1256"
         import="SoftwareEngineering.User"
         import="java.util.*"
         import="SoftwareEngineering.Group"
         %>
<!doctype html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html">
  <title>Health Tracker</title>
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
  <script src="javascript/javascript.js" type="text/javascript"></script>
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
</head>
<body>
    <%
            response.setHeader("Expires", "Sun, 7 May 1995 12:00:00 GMT");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");

            User user = (User) session.getAttribute("userSession");
            if (user == null) {
                response.sendRedirect("index.jsp");
            } else {
        %>
  <div id="topbar">
            <a style="float: left; padding-left: 10px;" href="Logout.jsp">[Log Out]</a>
            <a style="padding-left: 220px;" href="ProfilePage.jsp">Health Tracker</a>
            <form style="margin: 0; padding: 0; display: inline; float:right; padding-right: 10px;" action="listView" method="post">
            <input style="display: inline;" name="searchQ" type="text" class ="input" value ="Search for Group" />
            <select style="display: inline;" name="type" value="">
                                    <option value="All">All Groups</option>
                                    <option value="Diet Group">Diet Group</option>
                                    <option value="Gentle Exercise Group">Gentle Exercise Group</option>
                                    <option value="Extreme Exercise Group">Extreme Exercise Group</option>
            </select>
            <input style="display: inline;" type="submit" name="submit" value="searchGroup" class="button" />
            </form>  
        </div>
  
  <div id="w">
    <div id="content" class="clearfix">
      <section id="bio">
          <h1>List of Groups</h1>
          <%
                List groupList = (List) request.getAttribute("groupList");
                Iterator it = groupList.iterator();
                while(it.hasNext())
                {
                Group g = (Group) it.next();
                %>
                <hr>
                <p><a href="group_display?groupName=<%= g.getGroupName() %>"><%= g.getGroupName() %> - <%= g.getCatagory()%> - <%= g.getRating() %></a></p>
                <%
                }
                %>
      </section>
    </div><!-- @end #content -->
  </div><!-- @end #w -->
</body>
<%
    }
%>
</html>
</html>

