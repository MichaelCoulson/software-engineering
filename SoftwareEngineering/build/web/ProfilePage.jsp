
<%@page import="SoftwareEngineering.HealthOverview"%>
<%@page import="SoftwareEngineering.DietCapture"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" 
         contentType="text/html; charset=windows-1256"
         pageEncoding="windows-1256"
         import="SoftwareEngineering.User"
         import="java.util.*"
         import="SoftwareEngineering.Group"
         import="SoftwareEngineering.ExerciseGoal"
         import="SoftwareEngineering.WeightGoal"
         import="SoftwareEngineering.Food"
         import="SoftwareEngineering.Drink"
         import="SoftwareEngineering.HealthHistory"
         %>
<!DOCTYPE html>
<html>

    <head>
        <script type="text/javascript" src="javascript/canvasjs.js"></script>
        <script type="text/javascript" src="javascript/canvasjs.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <script src="javascript/javascript.js" type="text/javascript"></script>
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <title>Health Tracker</title>
    </head>
    <body>
        <%
            response.setHeader("Expires", "Sun, 7 May 1995 12:00:00 GMT");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");

            User user = (User) session.getAttribute("userSession");
            if (user == null) {
                response.sendRedirect("index.jsp");
            } else {
        %>
        
        <div id="topbar">
            <a style="float: left; padding-left: 10px;" href="Logout.jsp">[Log Out]</a>
            <a style="padding-left: 220px;" href="ProfilePage.jsp">Health Tracker</a>
            <form style="margin: 0; padding: 0; display: inline; float:right; padding-right: 10px;" action="listView" method="post">
                <input style="display: inline;" name="searchQ" type="text" class ="input" value ="Search for Group" />
                <select style="display: inline;" name="type" value="">
                    <option value="All">All Groups</option>
                    <option value="Diet Group">Diet Group</option>
                    <option value="Gentle Exercise Group">Gentle Exercise Group</option>
                    <option value="Extreme Exercise Group">Extreme Exercise Group</option>
                </select>
                <input style="display: inline;" type="submit" name="submit" value="searchGroup" class="button" />
            </form>  
        </div>

        <div id="w">
            <div id="content" class="clearfix">
                <div id="userphoto"><img src="images/avatar.png" alt="default avatar"></div>
                <h1><%= user.getFirstName()%> <%= user.getLastName()%></h1>
                <nav id="profiletabs">
                    <ul class="clearfix">
                        <li><a href="#bio" class="sel">Bio</a></li>
                        <li><a href="#setGoal">Set Goal</a></li>
                        <li><a href="#activity">Goals</a></li>
                        <li><a href="#History">history</a></li>
                        <li><a href="#friends">Groups</a></li>
                        <li><a href="#dc">Diet</a></li>
                        <li><a href="#settings">Settings</a></li>
                    </ul>
                </nav>

                <section id="bio">
                    <p>Rating: <%= user.getRating()%></p>
                    <p>Date of Birth: <%= user.getDob()%></p>
                    <p>Username: <%= user.getUsername()%></p>
                    <p>Gender: <%= user.getGender()%></p>
                    <p>Email: <%= user.getEmailAddress()%></p>
                    <p>Height (meters): <%= user.getHeight()%></p>   
                    <p>Weight (kilograms): <%= user.getWeight()%></p>
                    <p>BMI: <%= user.calculateBMI(user.getWeight(), user.getHeight())%></p>
                    <p>Current Health Status: <%= user.returnHealthStatus()%></p>
                    <p>Target Weight <%= user.getTargetWeight()%></p>
                    <%
                        //Checks if the person is overweight
                        if (user.getBmi() > 25) {%>
                    <p>Recommended weight loss - 
                        <%= user.generateHealth()%>kg</p>

                    <%} //Checks if the user is a healthy weight 
                        if (user.getBmi() < 18.5) {%>
                    <h1>Recommended weight gain - 
                        <%= user.generateHealth()%>kg</h1>

                    <%
                        }
                    %>

                </section>

                 <section id="History" class="hidden">

                    <%
                        int userID = user.getUserID();
                        int i = 1;
                        ArrayList hhlist = HealthHistory.returnAll(userID);
                        ArrayList wl = new ArrayList();
                        ArrayList bmi = new ArrayList();
                        if(hhlist.size() >= 5){
                            
                        Iterator it12 = hhlist.iterator();%>
                                 <table id="tftable" class="tftable" >

                        <tr>
                            <th><strong>Weight</strong></th>

                            <th><strong>Height</strong></th>

                            <th><strong>BMI</strong></th>

                            <th><strong>Date</strong></th>

                        </tr> <%
                        while (it12.hasNext()) {
                            HealthHistory hh = (HealthHistory) it12.next();
                    %>
                        <tr>
                            <td><%= hh.getWeight()%></td>

                            <td><%= hh.getHeight()%></td> 

                            <td><%= hh.getBmi()%></td> 

                            <td><%= hh.getDate()%></td> 

                        </tr>
                    
                    <%
                            wl.add(hh.getWeight());
                            bmi.add(hh.getBmi());
                        }
                        
                    %>
                    </table>
                    <script type="text/javascript">
                
                        if (<%=wl.size() >= 5 %>&& <%= bmi.size() >= 5%>) {
                            window.onload = function () {
                                var chart = new CanvasJS.Chart("chartContainer",
                                        {
                                            title: {
                                                text: "Weight Progress"
                                            },
                                            axisY: {
                                                title: "BMI, Weight(kg)",
                                                interlacedColor: "Azure",
                                                interval: 50,
                                                tickLength: 10
                                            },
                                            axisX: {
                                                title: "Last Five Days",
                                                interlacedColor: "#F0F8FF"
                                            },
                                            data: [{
                                                    type: "line", //or stackedColumn
                                                    dataPoints: [
                                                        {x: 1, y:<%= wl.get(wl.size() - 5)%>},
                                                        {x: 2, y:<%= wl.get(wl.size() - 4)%>},
                                                        {x: 3, y:<%= wl.get(wl.size() - 3)%>},
                                                        {x: 4, y:<%= wl.get(wl.size() - 2)%>},
                                                        {x: 5, y:<%= wl.get(wl.size() - 1)%>}
                                                    ]
                                                }, {
                                                    type: "line", //or stackedColumn
                                                    dataPoints: [
                                                        
                                                        {x: 1, y:<%= bmi.get(bmi.size() - 5)%>},
                                                        {x: 2, y:<%= bmi.get(bmi.size() - 4)%>},
                                                        {x: 3, y:<%= bmi.get(bmi.size() - 3)%>},
                                                        {x: 4, y:<%= bmi.get(bmi.size() - 2)%>},
                                                        {x: 5, y:<%= bmi.get(bmi.size() - 1)%>}
                                                
                                                    ]
                                                }
                                            ]
                                        });

                                chart.render();
                            }
                        }
                        
                        <%
                        }
                        %>
                       </script>


                    <br><br>                   
                    <div id="chartContainer" style="height: 500px; width: 700px;">
                    </div>

                </section>

                <section id="setGoal" class="hidden">
                    <button onclick="weight()">Weight Goal</button>        
                    <button onclick="exercise()">Exercise</button>  
                    <div id="weight" style="display:none"> 
                        <form id ="f1" name="input" action="setGoalController" method="post">
                            <fieldset>

                                <br>
                                <legend>Set Weight Goal:</legend>

                                <br>
                                Goal Name <br>
                                <input id="input2" type="text" name="weightName" size="30" value=""><br>        
                                <br>
                                
                                <%
                                    //Checks if the person is overweight
                                    if (user.getBmi() > 25) {%>
                                <p>Recommended weight loss goal - 
                                    <%= user.generateHealth()%>kg</p>

                                <%}%>


                                Target Weight Loss goal <br>
                                <select name="Weight" value="">
<<<<<<< HEAD
                                    <option> - Weight - </option>
                                                                       <option value="300">300 kg</option>
                                    <option value="290">290 kg</option>
                                    <option value="280">280 kg</option>
                                    <option value="270">270 kg</option>
                                    <option value="260">260 kg</option>
                                    <option value="250">250 kg</option>
                                    <option value="240">240 kg</option>
                                    <option value="230">230 kg</option>
                                    <option value="220">220 kg</option>
                                    <option value="210">210 kg</option>
                                    <option value="200">200 kg</option>
                                    <option value="190">190 kg</option>
                                    <option value="180">180 kg</option>
                                    <option value="170">170 kg</option>
                                    <option value="160">160 kg</option>
                                    <option value="150">150 kg</option>
                                    <option value="140">140 kg</option>
                                    <option value="130">130 kg</option>
                                    <option value="120">120 kg</option>
                                    <option value="110">110 kg</option>
                                    <option value="100">100 kg</option>
                                    <option value="90">90 kg</option>
                                    <option value="80">80 kg</option>
                                    <option value="70">70 kg</option>
                                    <option value="60">60 kg</option>
                                    <option value="50">50 kg</option>
                                    <option value="40">40 kg</option>
=======
                                    <option value="" selected="selected"> - Weight - </option>
                                    <option value="0.1">100 g</option>
                                    <option value="0.2">200 g</option>
                                    <option value="0.3">300 g</option>
                                    <option value="0.4">400 g</option>
                                    <option value="0.5">500 g</option>
                                    <option value="0.6">600 g</option>
                                    <option value="0.7">700 g</option>
                                    <option value="0.8">800 g</option>
                                    <option value="0.9">900 g</option>
                                    <option value="1">1.0 kg</option>
                                    <option value="1.1">1.1 kg</option>
                                    <option value="1.2">1.2 kg</option>
                                    <option value="1.3">1.3 kg</option>
                                    <option value="1.4">1.4 kg</option>
                                    <option value="1.5">1.5 kg</option>
                                    <option value="1.6">1.6 kg</option>
                                    <option value="1.7">1.7 kg</option>
                                    <option value="1.8">1.8 kg</option>
                                    <option value="1.9">1.9 kg</option>
                                    <option value="2">2.0 kg</option>
                                    <option value="2.1">2.1 kg</option>
                                    <option value="2.2">2.2 kg</option>
                                    <option value="2.3">2.3 kg</option>
                                    <option value="2.4">2.4 kg</option>
                                    <option value="2.5">2.5 kg</option>
                                    <option value="2.6">2.6 kg</option>
                                    <option value="2.7">2.7 kg</option>
                                    <option value="2.8">2.8 kg</option>
                                    <option value="2.9">2.9 kg</option>
                                    <option value="3.0">3.0 kg</option>
                                    <option value="3.1">3.1 kg</option>
                                    <option value="3.2">3.2 kg</option>
                                    <option value="3.3">3.3 kg</option>
                                    <option value="3.4">3.4 kg</option>
                                    <option value="3.5">3.5 kg</option>
                                    <option value="3.6">3.6 kg</option>
                                    <option value="3.7">3.7 kg</option>
                                    <option value="3.8">3.8 kg</option>
                                    <option value="3.9">3.9 kg</option>
                                    <option value="4">4.0 kg</option>
                                    <option value="4.1">4.1 kg</option>
                                    <option value="4.2">4.2 kg</option>
                                    <option value="4.3">4.3 kg</option>
                                    <option value="4.4">4.4 kg</option>
                                    <option value="4.5">4.5 kg</option>
                                    <option value="4.6">4.6 kg</option>
                                    <option value="4.7">4.7 kg</option>
                                    <option value="4.8">4.8 kg</option>
                                    <option value="4.9">4.9 kg</option>
                                    <option value="5">5.0 kg</option>
>>>>>>> e03829d9da801dd79c85ca62a5c0d76ef42b95a3
                                </select>


                                </select>
                                <br>
                                Target Date <br>
                                <select name="DayW" value="">
                                    <option value="" selected="selected"> - Day - </option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                                <select name="MonthW" value="">
                                    <option value="" selected="selected"> - Month - </option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <select name="YearW" value="">
                                    <option value="" selected="selected"> - Year - </option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>

                                </select>
                                <br>
                                <br>
                                <input id="input7" name="setGoalController" type="submit" value="Set Goal!">
                            </fieldset>
                        </form>
                    </div>                        
                    <div id="exercise" style="display:none">        

                        <form id ="f1" name="input" action="setGoalControllerExercise" method="post">
                            <fieldset>

                                <br>
                                <legend>Set Exercise: </legend>
                                <br>
                                Exercise Name <br>
                                <input id="input2" type="text" name="exerciseName" size="30" value=""><br>        
                                <br>
                                <select name="Exercise" value="">
                                    <option value="" selected="selected"> - Exercise - </option>
                                    <option value="walk">Walk</option>
                                    <option value="run">Run</option>
                                    <option value="cycling">Cycling</option>
                                    <option value="rowing">Rowing</option>
                                    <option value="swimming">Swimming</option>
                                </select><br>

                                <select name="Distance" value="">
                                    <option value="" selected="selected"> - Distance - </option>
                                    <option value="100">100 m</option>
                                    <option value="200">200 m</option>
                                    <option value="400">300</option>
                                    <option value="400">400</option>
                                    <option value="500">500m</option>
                                    <option value="600">600m</option>
                                    <option value="700">700m</option>
                                    <option value="800">800m</option>
                                    <option value="900">900m</option>
                                    <option value="1000">1000m</option>
                                    <option value="1500">1500m</option>
                                    <option value="2500">2500m</option>
                                    <option value="21097">Half Marathon</option>
                                    <option value="42195">Marathon</option>
                                </select>
                                <br>

                                <br>
                                Target duration <br>
                                <input id="input222" type="number" min="0" name="Duration" size="30" value=""><br>        
                                <br>

                                Target time <br>
                                <input id="input2" type="number" min="0" name="targetTime" size="30" value=""><br>        
                                <br>
                                Target Date <br>
                                <select name="DayE" value="">
                                    <option value="" selected="selected"> - Day - </option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                                <select name="MonthE" value="">
                                    <option value="" selected="selected"> - Month - </option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <select name="YearE" value="">
                                    <option value="" selected="selected"> - Year - </option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                </select>
                                <br>
                                <br>

                                <input id="input7" name="setGoalControllerExercise" type="submit" value="Set Goal!">

                            </fieldset>
                        </form>
                    </div>  
                </section>

                <section id="activity" class="hidden">
                    <br><br><br>
                    <h2>Weight Goals</h2>

                    <table class="tftable">
                        <tr>
                            <th>Goal Name</th>
                            <th>Target Date</th>
                            <th>Target Weight</th>
                            <th>Completion Date</th>
                            <th>Status</th>
                        </tr>
                        <tr>
                            <%
                                user.updateWeightGoals();
                                Iterator<WeightGoal> weightIterator = user.getWgoal().iterator();
                                while (weightIterator.hasNext()) {
                                    SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
                                    WeightGoal thisWeightGoal = weightIterator.next();
                                    String goalName = thisWeightGoal.getName();
                                    String tar = thisWeightGoal.getTargetDate();
                                    boolean achieved = thisWeightGoal.isGoalAchieved();
                                    String comp = thisWeightGoal.getCompletionDate();
                                    double targetWeight = thisWeightGoal.getTarget();
                                    Date targetDate = formatDate.parse(tar);
                                    Date today = new Date();
                            %>
                            <td><% out.println(goalName);%></td>
                            <td><% out.println(tar);%></td> 
                            <td><% out.println(targetWeight);%> kg</td>
                            <td><%out.println(comp);%></td> 
                            <td><% if (today.after(targetDate) && (achieved == false)) {
                                    out.println("Expired");
                                } else if (today.before(targetDate) && (achieved == true)) {
                                    out.println("Achieved!");
                                } else {
                                    out.println("Current");
                                } %></td>
                        </tr>
                        <%
                            }
                        %>
                    </table>

                    <br>
                    <h2>Exercise Goals</h2>



                    <table class="tftable">
                        <tr>
                            <th>Goal Name</th>
                            <th>Target Date</th>
                            <th>Goal Type</th>
                            <th>Distance</th>
                            <th>Time taken</th>
                            <th>Completion Date</th>
                            <th>Completed?</th>
                        </tr>
                        <tr>
                            <%
                                user.updateExerciseGoals();
                                Iterator<ExerciseGoal> goalIterator = user.getEgoal().iterator();

                                while (goalIterator.hasNext()) {
                                    ExerciseGoal thisGoal = goalIterator.next();
                                    String goalName = thisGoal.getName();
                                    String tar = thisGoal.getTargetDate();
                                    String comp = thisGoal.getCompletionDate();
                                    String type = thisGoal.getType();
                                    double timeTaken = thisGoal.getCompletionTime();
                                    double distance = thisGoal.getDistance();
                            %>
                        <tr>
                            <td valign="center"><% out.println(goalName);%></td>
                            <td valign="center"><% out.println(tar);%></td> 
                            <td valign="center"><% out.println(type);%></td>
                            <td valign="center"><% out.println(distance); %></td>
                            <% if (thisGoal.isGoalAchieved() == true) {%>
                            <td valign="center"><%out.println(timeTaken);%></td>
                            <td valign="center"><%out.println(comp);%></td>
                            <td valign="center"><%out.println("Completed!");%> 
                                <a href="setGoalControllerExercise?exerciseName=<%out.println(goalName);%>HARDER&Distance=<% out.println(distance); %>&targetTime=<% out.println(timeTaken);%>&Exercise=<% out.println(type);%>">[Harder!]</a></td>
                        </tr>
                        <% } else {
                        %>
                        <td colspan="3">
                            <form id ="f1" name="input" action="updateExerciseGoal" method="post">
                                <input type="hidden" name="groupID" value="<%=thisGoal.getExerciseID()%>">
                                Date Completed: <select name="Day" style="width: 60px;"><br>
                                    <option> - Day - </option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                                <select name="Month">
                                    <option> - Month - </option>
                                    <option value="01">January</option>
                                    <option value="02">February</option>
                                    <option value="03">March</option>
                                    <option value="04">April</option>
                                    <option value="05">May</option>
                                    <option value="06">June</option>
                                    <option value="07">July</option>
                                    <option value="08">August</option>
                                    <option value="09">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                                <select name="Year" style="width: 60px;">
                                    <option> - Year - </option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>

                                </select><br>
                                Time taken: (mins) <input type="text" name="timeTaken" style="width: 50px;" />
                                <input type="submit" name="submit" value="Update!" class="button" />
                            </form>
                        </td>
                        </tr>
                        <%}
                            }%>

                    </table>
                </section>

                <section id="friends" class="hidden">
                    <button onclick="reveal3()">Add A Group</button>
                    <div id="makeG" style="display: none">
                        <form name="new_Group" action="CreateGroup" method="post">
                            <input name="groupName" type="text" class ="input" value ="Unique Group Name" />
                            <select name="groupType">
                                <option value="Diet Group">Diet Group</option>
                                <option value="Gentle Exercise Group">Gentle Exercise Group</option>
                                <option value="Extreme Exercise Group">Extreme Exercise Group</option>
                            </select>
                            <div class="footer">
                                <br>
                                <input type="submit" name="submit" value="Add Group" class="button" />
                            </div>
                        </form>  
                    </div>
                    <br><br><br>
                    <h1>User Groups:</h1>
                    <%
                        user.updateUserGroups();
                        Iterator<Group> iterator = user.getGroups().iterator();

                        while (iterator.hasNext()) {
                            String groupName = iterator.next().getGroupName();
                    %>
                    <ul id="friendslist" class="clearfix">
                        <p>
                        <li><a href="group_display?groupName=<%= groupName%>"><img src="images/avatar.png" width="22" height="22"> <% out.println(groupName);%></a> </li>
                        <a href="inviteController?inviteResponse=false&inviteGroup=<%=groupName%>">[Leave Group]</a>

                    </ul>
                    <%
                        }
                    %>

                    <h1> Pending Groups:</h1>

                    <%
                        user.updatePendingGroups();
                        Iterator<Group> iteratorGroups = user.getPendingGroups().iterator();

                        while (iteratorGroups.hasNext()) {
                            String inviteGroupName = iteratorGroups.next().getGroupName();
                    %>
                    <ul id="friendslist" class="clearfix">
                        <p>
                        <li><a href="#"><img src="images/avatar.png" width="22" height="22"> <% out.println(inviteGroupName);%></a> </li>
                        <a href="inviteController?inviteResponse=true&inviteGroup=<%=inviteGroupName%>">[Accept</a> / 
                        <a href="inviteController?inviteResponse=false&inviteGroup=<%=inviteGroupName%>">Decline]</a>
                        </p>

                    </ul>
                    <%
                        }
                    %>

                </section>

                <section id="dc" class="hidden">
                    <br>
                    <br>Add Food<br>
                    <button onclick="addFood()">Food</button>                 

                    <div id="food" style="display:none"> 
                        <form id ="f1" name="input" action="addFoodController" method="post">
                            <fieldset>
                                <legend>Add Food:</legend>

                                <br>
                                Food Name<br>
                                <input id="input2" type="text" name="foodName" size="30" value="" required= "required"><br>        
                                <br>
                                Calories <br>
                                <input id="input2" type="number" name="calories" size="30" value="" required= "required"><br>        
                                <br>


                                <br>

                                <br>
                                <input id="input7" name="addFoodController" type="submit" value="Add Food!">
                            </fieldset>
                        </form>
                        ${message}
                    </div>                
                    <br>Add Drink<br>
                    <button onclick="addDrink()">Drink</button>          
                    <div id="drink" style="display:none">        

                        <form id ="f1" name="input" action="addDrinkController" method="post">
                            <fieldset>
                                <legend>Set Exercise: </legend>
                                <br>
                                Drink Name <br>
                                <input id="input2" type="text" name="drinkName" size="30" value="" required= "required"><br>        
                                <br>
                                calories <br>
                                <input id="input2" type="number" name="calories" size="30" value="" required= "required"><br>        
                                <br>
                                <br>

                                <input id="input7" name="addDrinkController" type="submit" value="Add Drink!">

                            </fieldset>
                        </form>
                        ${message}
                    </div>  

                    <br>Add Meal<br>
                    <button onclick="addMeal()">Add Meal</button>          
                    <div id="meal" style="display:none">        

                        <form id ="f1" name="input" action="addMealController" method="post">
                            <fieldset>
                                <legend>Add Meal: </legend>
                                <br>
                                Meal type <br>
                                <select name="mealName" value="" required= "required">
                                    <option> - Meal Type - </option>
                                    <option value="Breakfast">Breakfast</option>
                                    <option value="Lunch    ">Lunch</option>
                                    <option value="Dinner   ">Dinner</option>
                                    <option value="Snack    ">Snack</option>
                                </select><br>        
                                <br>
                                Food <br>
                                <select name="fName" required= "required">
                                    <%
                                        ArrayList foodList = Food.returnAll(userID);
                                        Iterator it = foodList.iterator();
                                        while (it.hasNext()) {
                                            Food f = (Food) it.next();
                                    %>
                                    <option value="<%= f.getName()%>,<%= f.getCalories()%>"><%= f.getName()%> <%= f.getCalories()%> </option>
                                    <%
                                        }
                                    %>
                                </select>
                                <br>
                                Drink <br>
                                <select name="dName" required= "required">
                                    <%
                                        ArrayList drinkList = Drink.returnAll(userID);
                                        Iterator dit = drinkList.iterator();
                                        while (dit.hasNext()) {
                                            Drink d = (Drink) dit.next();
                                    %>
                                    <option value="<%= d.getName()%>,<%= d.getCalories()%>"><%= d.getName()%> <%= d.getCalories()%> </option>
                                    <%
                                        }
                                    %>
                                </select>
                                <br>
                                <br> 

                                <input id="input7" name="addMealController" type="submit" value="Add Meal!">

                            </fieldset>
                        </form>


                    </div>  

                    <h1>Meal History:</h1>
                    <%
                        ArrayList mealList = DietCapture.returnAll(userID);
                        Iterator mealIt = mealList.iterator();
                        DietCapture dc = new DietCapture();
                        double totalMealCals = 0;
                        while (mealIt.hasNext()) {
                            dc = (DietCapture) mealIt.next();
                            totalMealCals += dc.getTotalCals();
                    %>


                    <table id="tftable" class="tftable" >

                        <tr>
                            <th><strong>Meal Type</strong></th>

                            <th><strong>Food</strong></th>

                            <th><strong>Drink</strong></th>

                            <th><strong>Calories</strong></th>

                        </tr>

                        <tr>
                            <td><%= dc.getName()%></td>

                            <td><%= dc.getF()%></td> 

                            <td><%= dc.getD()%></td> 

                            <td><%= dc.getTotalCals()%></td> 

                        </tr>
                    </table>
                    <%
                        }
                    %>

                    <h1>Total Calories to date:</h1><h1><%= dc.getTotalCals()%></h1> 



                </section>



                <section id="settings" class="hidden">
                    <p>Edit your settings:</p>

                    <p class="setting"><span>Height <img src="images/edit.png" alt="*Edit*" onclick="reveal1()"></span> <%= user.getHeight()%> m</p>
                    <p class="setting"><span>Weight <img src="images/edit.png" alt="*Edit*" onclick="reveal2()"></span> <%= user.getWeight()%> kg</p>

                    <form action="editController" method="post"> 

                        <div id="editHeight" style="display: none">       
                            <p>Edit Height</p>
                            <input type="number" min="0" name="height" size="30" step="0.01" value="<%= user.getHeight()%>"> 
                        </div>
                        <div id="editWeight" style="display: none"> 
                            <p>Edit Weight</p>
                            <input type="number" min="0" name="weight" size="30" step="0.01" value="<%= user.getWeight()%>">    
                        </div>
                        <div id="editSave" style="display: none">
                            <br>
                            <input type="submit" value="Save Edits"/>
                        </div>
                    </form>
                </section>
            </div>
        </div>
        <%
            }
        %>
        <script type="text/javascript">
            $(function () {
                $('#profiletabs ul li a').on('click', function (e) {
                    e.preventDefault();
                    var newcontent = $(this).attr('href');

                    $('#profiletabs ul li a').removeClass('sel');
                    $(this).addClass('sel');

                    $('#content section').each(function () {
                        if (!$(this).hasClass('hidden')) {
                            $(this).addClass('hidden');
                        }
                    });

                    $(newcontent).removeClass('hidden');
                });
            });
        </script>
    </body>
</html>
