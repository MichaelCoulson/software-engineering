<!doctype html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html">
  <title>Health Tracker</title>
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
  <script src="javascript/javascript.js" type="text/javascript"></script>
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
</head>
<body>
  <div id="topbar">
  <a href="index.jsp">Health Tracker</a>
  </div>
  
  <div id="w">
    <div id="content" class="clearfix">
      <h1>Home</h1>
      
      <section id="bio">
         <form action="LoginServlet" method="post">
                         <h3>Email: </h3>    <input type="email" style="font-size:14px;width:160px;height:23px"  name="Email" value="Email" >
                       <h3>Password: </h3>    <input type="password" style="font-size:14px;width:160px;height:23px"  name="Password" value="Password" />
                       <br><input name=".btn" class="btn" type="submit" id="loginbutton" value="Login" />
         </form><br>
         ${message}
          <h1><a href="Signup.jsp">Sign Up</a></h1>
          <p>Disclaimer: This application is not a commercial application and does not provide
insurance. This is a study project that is part of a Computing Science module taught at the
University of East Anglia, Norwich, UK. If you have any questions, please contact the
module coordinator, Joost Noppen, at j.noppen@uea.ac.uk</p>
      </section>
    </div><!-- @end #content -->
  </div><!-- @end #w -->
</body>
</html>
</html>

