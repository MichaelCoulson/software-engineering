<%@ page language="java" 
         contentType="text/html; charset=windows-1256"
         pageEncoding="windows-1256"
         import="SoftwareEngineering.User"
         import="java.util.*"
         import="SoftwareEngineering.Group"
         import="SoftwareEngineering.WeightGoal"
         %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <link href='http://fonts.googleapis.com/css?family=Duru+Sans|Droid+Sans:400,700' rel='stylesheet' type='text/css'/>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <title>Health Tracker - LogOut!</title>
    </head>
    <body>
        <%
            response.setHeader("Expires", "Sun, 7 May 1995 12:00:00 GMT");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");

            User user = (User) session.getAttribute("userSession");
            if (user == null) {
                response.sendRedirect("index.jsp");
            } else {
        %>
        <h1><%= user.getFirstName()%>, Please Confirm  Logout!</h1>
        <h2> <a href = "Logout" > Log me Out!</a > </h2>
        <%
            }
        %>
    </body>
</html >