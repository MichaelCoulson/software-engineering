<!doctype html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html">
  <title>Health Tracker</title>
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
  <script src="javascript/javascript.js" type="text/javascript"></script>
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
</head>
<body>
  <div id="topbar">
  <a href="index.jsp">Health Tracker</a>
  </div>
  
  <div id="w">
    <div id="content" class="clearfix">
      <h1>Sign Up</h1>
      
      <section id="bio">
    
          <h2>Username already exists!

<button onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
</h2>
        
      </section>  
    </div><!-- @end #content -->
  </div><!-- @end #w -->
</body>
</html>
</html>

