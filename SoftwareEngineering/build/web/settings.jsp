<%@page import="SoftwareEngineering.DietCapture"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" 
         contentType="text/html; charset=windows-1256"
         pageEncoding="windows-1256"
         import="SoftwareEngineering.User"
         import="java.util.*"
         import="SoftwareEngineering.Group"
         import="SoftwareEngineering.ExerciseGoal"
         import="SoftwareEngineering.WeightGoal"
         import="SoftwareEngineering.Food"
         import="SoftwareEngineering.Drink"
         %>
<!DOCTYPE html>
<html>

    <head>
        <script type="text/javascript" src="javascript/canvasjs.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <script src="javascript/javascript.js" type="text/javascript"></script>
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <title>Health Tracker</title>
    </head>
    <body>
        <%
            response.setHeader("Expires", "Sun, 7 May 1995 12:00:00 GMT");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");

            User user = (User) session.getAttribute("userSession");
            if (user == null) {
                response.sendRedirect("index.jsp");
            } else {
        %>
       
        <div id="topbar">
            <a style="float: left; padding-left: 10px;" href="Logout.jsp">[Log Out]</a>
            <a style="padding-left: 220px;" href="settings.jsp">Health Tracker</a>
           
        </div>

        <div id="w">
            <div id="content" class="clearfix">
                <div id="userphoto"><img src="images/avatar.png" alt="default avatar"></div>
                <h1><%= user.getFirstName()%> <%= user.getLastName()%></h1>
                <nav id="profiletabs">
                    <ul class="clearfix">
                        <li><a href="#settings">Bio</a></li>
                        <li><a href="#settings">Set Goal</a></li>
                        <li><a href="#settings">Goals</a></li>
                        <li><a href="#settings">Groups</a></li>
                        <li><a href="#settings">Diet Capture</a></li>
                        <li><a href="#settings" class="sel">Settings</a></li>
                    </ul>
                </nav>

                
                <section id="settings">
                    <p>Edit your settings:</p>

                    <p class="setting"><span>Height <img src="images/edit.png" alt="*Edit*" onclick="reveal1()"></span> <%= user.getHeight()%> m</p>
                    <p class="setting"><span>Weight <img src="images/edit.png" alt="*Edit*" onclick="reveal2()"></span> <%= user.getWeight()%> kg</p>

                    <form action="editController" method="post"> 

                        <div id="editHeight" style="display: none">       
                            <p>Edit Height</p>
                            <textarea name="height" cols="30" rows="1"><%= user.getHeight()%> </textarea>   
                        </div>
                        <div id="editWeight" style="display: none"> 
                            <p>Edit Weight</p>
                            <textarea name="weight" cols="30" rows="1"><%= user.getWeight()%> </textarea>   
                        </div>
                        <div id="editSave" style="display: none">
                            <br>
                            <input type="submit" value="Save Edits"/>
                        </div>
                    </form>
                </section>
            </div>
        </div>
        <%
            }
        %>
        
    </body>
</html>
