function weight() {
    $("#weight").css("display", "block");
    $("#updateWeight").css("display", "none");
    $("#exercise").css("display", "none");
    $("#updateExercise").css("display", "none");  
}

function updateWeight(){
    $("#weight").css("display", "none");
    $("#updateWeight").css("display", "block");  
    $("#exercise").css("display", "none");
    $("#updateExercise").css("display", "none");  
}

function exercise(){
    $("#exercise").css("display", "Block");
    $("#updateExercise").css("display", "none");  
    $("#weight").css("display", "none");
    $("#updateWeight").css("display", "none");
}

function updateExercise(){
    $("#exercise").css("display", "none");
    $("#updateExercise").css("display", "block");  
    $("#weight").css("display", "none");
    $("#updateWeight").css("display", "none");
}

function addFood(){
    $("#food").css("display", "block");
    $("#drink").css("display", "none");  
    $("#meal").css("display", "none");
}

function addDrink(){ 
    $("#food").css("display", "none");
    $("#drink").css("display", "block");  
    $("#meal").css("display", "none");
}

function addMeal(){
    $("#food").css("display", "none");
    $("#drink").css("display", "none");  
    $("#meal").css("display", "block");
}


function reveal1() {
    $("#editWeight").css("display", "none");
    $("#editHeight").css("display", "block");
    $("#editSave").css("display", "block");
}

function reveal2() {
    $("#editHeight").css("display", "none");
    $("#editWeight").css("display", "block");
    $("#editSave").css("display", "block");
}

function reveal3() {
    $("#makeG").css("display", "block");
}

function revealmh() {
    $("#metrich").css("display", "block");
    $("#imperialh").css("display", "none");
    $('input[name="Height1"]').val('');
    $("#1").css("display", "block");
    $("#2").css("display", "none");
}
function revealih() {
    $("#metrich").css("display", "none");
    $('input[name="Height"]').val('');
    $("#imperialh").css("display", "block");
    $("#1").css("display", "none");
    $("#2").css("display", "block");
}
function revealmw() {
    $("#metricw").css("display", "block");
    $("#imperialw").css("display", "none");
    $('input[name="Weight1"]').val('');
    $("#metricw1").css("display", "block");
    $("#imperialw1").css("display", "none");
    $("#imperialw1").val('');
    $("#3").css("display", "block");
    $("#4").css("display", "none");
}
function revealiw() {
    $("#metricw").css("display", "none");
    $('input[name="Weight"]').val('');
    $("#imperialw").css("display", "block");
    $("#metricw1").css("display", "none");
    $("#metricw1").val('');
    $("#imperialw1").css("display", "block");
    $("#3").css("display", "none");
    $("#4").css("display", "block");
}

